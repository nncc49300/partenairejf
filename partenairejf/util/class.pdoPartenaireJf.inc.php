﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application JardiPlants
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
*/

class PdoPartenaireJf
{   		
      	private static $serveur='mysql:host=mysql-partenairejf.alwaysdata.net';
      	private static $bdd='dbname=partenairejf_db';   		
      	private static $user='117351' ;    		
      	private static $mdp='M@mltc21mr17st' ;	
		private static $monPdo;
		private static $monPdoPartenaireJf = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
    		PdoPartenaireJf::$monPdo = new PDO(PdoPartenaireJf::$serveur.';'.PdoPartenaireJf::$bdd, PdoPartenaireJf::$user, PdoPartenaireJf::$mdp); 
			PdoPartenaireJf::$monPdo->query("SET CHARACTER SET utf8");
			PdoPartenaireJf::$monPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	public function _destruct(){
		PdoPartenaireJf::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : $instancePdoPartenaireJf = PdoPartenaireJf::getPdoPartenaireJf();
 * @return l'unique objet de la classe PdoPartenaireJf
 */
	public  static function getPdoPartenaireJf()
	{
		if(PdoPartenaireJf::$monPdoPartenaireJf == null)
		{
			PdoPartenaireJf::$monPdoPartenaireJf= new PdoPartenaireJf();
		}
		return PdoPartenaireJf::$monPdoPartenaireJf;  
	}

	public function getLesSections()
	{
		$req="SELECT * FROM section";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesSections=$res->fetchAll();
		return $lesSections;
	}

	public function getLesTypePartenaires()
	{
		$req="SELECT * FROM typepartenaire ORDER BY nomTypePartenaire";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesTypePartenaires=$res->fetchAll();
		return $lesTypePartenaires;
	}

	public function getLesCivilites()
	{
		$req="SELECT * FROM civilitecontact";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesCivilites=$res->fetchAll();
		return $lesCivilites;
	}

	public function getLesPrestations()
	{
		$req="SELECT * FROM prestation";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesPrestations=$res->fetchAll();
		return $lesPrestations;
	}

	public function getLesSaisons()
	{
		$req="SELECT * FROM saison";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesSaisons=$res->fetchAll();
		return $lesSaisons;
	}

	public function getLesDurees()
	{
		$req="SELECT * FROM duree";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesDurees=$res->fetchAll();
		return $lesDurees;
	}

	public function getLesPartenaires()
	{
		
		$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison ORDER BY nomTypePartenaire";

		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesPartenaires=$res->fetchAll();
		return $lesPartenaires;
		
	}

	public function getLesPartenairesFiltre($filtreSection,$filtreTypePartenaire,$filtreSaison)
	{
		if($filtreSection=="0" AND $filtreSaison=="0" AND $filtreTypePartenaire=="0"){
			//requete d'aucun
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison ORDER BY nomTypePartenaire";
		}

		
		if($filtreSection=="0" AND $filtreSaison=="0" AND $filtreTypePartenaire!="0"){
			//requete filtreTypePartenaire
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where typePartenaire_id='$filtreTypePartenaire' ORDER BY nomTypePartenaire";
		}

		if($filtreSection=="0" AND $filtreTypePartenaire=="0" AND $filtreSaison!="0"){
			//requete filtreSaison
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where saison_id='$filtreSaison' ORDER BY nomTypePartenaire";
		}
		

		if($filtreTypePartenaire=="0" AND $filtreSaison=="0" AND $filtreSection!="0"){
			//requete filtreSection
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where section_id='$filtreSection' ORDER BY nomTypePartenaire";
		}

		if($filtreSaison=="0" AND $filtreSection!="0" AND $filtreTypePartenaire!="0"){
			//requete filtreSection et filtreTypePartenaire
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where section_id='$filtreSection' AND typePartenaire_id='$filtreTypePartenaire' ORDER BY nomTypePartenaire";
		}

		if($filtreSection=="0" AND $filtreSaison!="0" AND $filtreTypePartenaire!="0"){
			//requete filtreSaison et filtreTypePartenaire
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where saison_id='$filtreSaison' AND typePartenaire_id='$filtreTypePartenaire' ORDER BY nomTypePartenaire";
		}

		if($filtreTypePartenaire=="0" AND $filtreSaison!="0" AND $filtreSection!="0"){
			//requete filtreSaison et filtreSection
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where saison_id='$filtreSaison' AND section_id='$filtreSection' ORDER BY nomTypePartenaire";
		}

		if($filtreSaison!="0" AND $filtreSection!="0" AND $filtreTypePartenaire!="0"){
			//requete des trois
			$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN typepartenaire ON partenaire.typePartenaire_id = typepartenaire.idTypePartenaire INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree INNER JOIN saison ON partenaire.saison_id = saison.idSaison Where saison_id='$filtreSaison' AND typePartenaire_id='$filtreTypePartenaire' AND section_id='$filtreSection' ORDER BY nomTypePartenaire";
		}
		
		$res=PdoPartenaireJf::$monPdo->query($req);
		$lesPartenaires=$res->fetchAll();
		return $lesPartenaires;
	}



	public function ajoutPartenaire($section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis)
	{
		$sql = PdoPartenaireJf::$monPdo->prepare("INSERT INTO partenaire (section_id, typePartenaire_id, civiliteContact_id, nomContact, fonctionContact, adresse, CP, ville, tel, mail, prestation, saison_id, prestation_id, duree_id, remarques, montant, datePaiement, montantPromis) VALUES (:section_id, :typePartenaire_id, :civiliteContact_id, :nomContact, :fonctionContact, :adresse, :CP, :ville, :tel, :mail, :prestation, :saison_id, :prestation_id, :duree_id, :remarques, :montant, :datePaiement, :montantPromis)");

		$sql->bindParam(':section_id', $section_id, PDO::PARAM_INT);
		$sql->bindParam(':typePartenaire_id', $typePartenaire_id, PDO::PARAM_STR);
		$sql->bindParam(':civiliteContact_id', $civiliteContact_id, PDO::PARAM_INT);
		$sql->bindParam(':nomContact', $nomContact, PDO::PARAM_STR);
		$sql->bindParam(':fonctionContact', $fonctionContact, PDO::PARAM_STR);
		$sql->bindParam(':adresse', $adresse, PDO::PARAM_STR);
		$sql->bindParam(':CP', $CP, PDO::PARAM_INT);
		$sql->bindParam(':ville', $ville, PDO::PARAM_STR);
		$sql->bindParam(':tel', $tel, PDO::PARAM_INT);
		$sql->bindParam(':mail', $mail, PDO::PARAM_STR);
		$sql->bindParam(':prestation', $prestation, PDO::PARAM_STR);
		$sql->bindParam(':saison_id', $saison, PDO::PARAM_STR);
		$sql->bindParam(':prestation_id', $prestation_id, PDO::PARAM_INT);
		$sql->bindParam(':duree_id', $duree_id, PDO::PARAM_INT);
		$sql->bindParam(':remarques', $remarques, PDO::PARAM_STR);
		$sql->bindParam(':montant', $montant, PDO::PARAM_STR);
		$sql->bindParam(':datePaiement', $datePaiement, PDO::PARAM_STR);
		$sql->bindParam(':montantPromis', $montantPromis, PDO::PARAM_INT);
		$sql->execute();
	}

	public function getUnPartenaire($idPartenaire)
	{
		$req="SELECT * FROM partenaire INNER JOIN section ON partenaire.section_id = section.idSection INNER JOIN civilitecontact ON partenaire.civiliteContact_id = civilitecontact.idCiviliteContact INNER JOIN prestation ON partenaire.prestation_id = prestation.idPrestation INNER JOIN duree ON partenaire.duree_id = duree.idDuree Where idPartenaire='$idPartenaire'";
		$res=PdoPartenaireJf::$monPdo->query($req);
		$unPartenaire=$res->fetch();
		return $unPartenaire;
	}

	public function modifierPartenaire($idPartenaire, $section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis)
	{
		$sql = PdoPartenaireJf::$monPdo->prepare("UPDATE partenaire SET section_id=:section_id, typePartenaire_id=:typePartenaire_id, civiliteContact_id=:civiliteContact_id, nomContact=:nomContact, fonctionContact=:fonctionContact, adresse=:adresse, CP=:CP, ville=:ville, tel=:tel, mail=:mail, prestation=:prestation, prestation_id=:prestation_id, saison_id=:saison_id, duree_id=:duree_id, remarques=:remarques, montant=:montant, datePaiement=:datePaiement, montantPromis=:montantPromis Where idPartenaire=:idPartenaire");
		$sql->bindParam(':idPartenaire', $idPartenaire, PDO::PARAM_STR);
		$sql->bindParam(':section_id', $section_id, PDO::PARAM_INT);
		$sql->bindParam(':typePartenaire_id', $typePartenaire_id, PDO::PARAM_STR);
		$sql->bindParam(':civiliteContact_id', $civiliteContact_id, PDO::PARAM_INT);
		$sql->bindParam(':nomContact', $nomContact, PDO::PARAM_STR);
		$sql->bindParam(':fonctionContact', $fonctionContact, PDO::PARAM_STR);
		$sql->bindParam(':adresse', $adresse, PDO::PARAM_STR);
		$sql->bindParam(':CP', $CP, PDO::PARAM_INT);
		$sql->bindParam(':ville', $ville, PDO::PARAM_STR);
		$sql->bindParam(':tel', $tel, PDO::PARAM_INT);
		$sql->bindParam(':mail', $mail, PDO::PARAM_STR);
		$sql->bindParam(':prestation', $prestation, PDO::PARAM_STR);
		$sql->bindParam(':prestation_id', $prestation_id, PDO::PARAM_INT);
		$sql->bindParam(':saison_id', $saison, PDO::PARAM_INT);
		$sql->bindParam(':duree_id', $duree_id, PDO::PARAM_INT);
		$sql->bindParam(':remarques', $remarques, PDO::PARAM_STR);
		$sql->bindParam(':montant', $montant, PDO::PARAM_STR);
		$sql->bindParam(':datePaiement', $datePaiement, PDO::PARAM_STR);
		$sql->bindParam(':montantPromis', $montantPromis, PDO::PARAM_STR);
		$sql->execute();
	}

	public function supprimerPartenaire($idPartenaire)
	{
		$req="DELETE FROM partenaire Where idPartenaire='$idPartenaire'";
		$res=PdoPartenaireJf::$monPdo->query($req);
	}


	public function ajoutTypePartenaire($nomTypePartenaire)
	{
		$sql = PdoPartenaireJf::$monPdo->prepare("INSERT INTO typepartenaire (nomTypePartenaire) VALUES (:nomTypePartenaire)");
		$sql->bindParam(':nomTypePartenaire', $nomTypePartenaire, PDO::PARAM_INT);
		$sql->execute();
	}

	public function ajoutSection($nomSection)
	{
		$sql = PdoPartenaireJf::$monPdo->prepare("INSERT INTO section (nomSection) VALUES (:nomSection)");
		$sql->bindParam(':nomSection', $nomSection, PDO::PARAM_INT);
		$sql->execute();
	}

	public function verifCode($login)
	{
		$req = PdoPartenaireJf::$monPdo->query('SELECT * FROM codeacces WHERE code = "'.$login.'" ');
		$resultat = $req->fetch();
		return $resultat;
	}

}
?>