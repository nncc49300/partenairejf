﻿<script type="text/javascript">
	function export2CSV(){

	  $("#table2excel").table2excel({
	    // exclude CSS class
	    exclude: ".noExl",
	    name: "Worksheet Name",
	    filename: "tableau.xls" //do not include extension
	  });

	}
</script>



<h2>Tableau de gestion des partenaires JF</h2><a class="btn btn-primary" href="index.php" style="margin: 0 0 2% 3%;">Accueil</a>
<a class="btn btn-warning" href="index.php?uc=deconnexion" style="margin: 0 0 2% 3%;">Déconnexion</a>
<a href="index.php?uc=gestionPartenaire&action=ajoutPartenaire"><button class="btn btn-success btn-ajoutContrat">Ajouter un nouveau contrat</button></a>
<a href="res/contratMecenat.doc"><button class="btn btn-info btn-ajoutContrat">Doc Mecenat</button></a>
<a href="res/contratSponsoring.doc"><button class="btn btn-info btn-ajoutContrat">Doc Sponsoring</button></a>
<a href="res/bookPartenaires.pdf" target="_blank"><button class="btn btn-info btn-ajoutContrat">Book partenaires</button></a>
<button class="btn btn-info btn-ajoutContrat btn-csv" onclick="export2CSV();">Générer CSV</button>

<table class="table table-hover table-striped table-border fixed" id="table2excel">
	<tr>
		<th class="noExl">Copier</th>
		<th width="10%">
		<select name="section_id" id='section_id' onchange="javascript:location.href='index.php?uc=affichage&action=affichageTableau&filtre=section&value='+this.value;">
			<OPTION value="0" <?php if($_SESSION['filtreSection']=="0"){echo "selected";} ?>>Sections</OPTION>
			<?php 
			foreach ($lesSections as $uneSection)
			{
			?>
				<option value="<?php echo $uneSection['idSection'];?>" <?php if(isset($_SESSION['filtreSection'])){if($_SESSION['filtreSection']==$uneSection['idSection']){echo "selected";}} ?>> <?php echo $uneSection['nomSection']; ?></option>
			<?php
			}
			?>
		</select>
		</th>
		<th width="10%">
		<select name="typePartenaire_id" id='typePartenaire_id' onchange="javascript:location.href ='index.php?uc=affichage&action=affichageTableau&filtre=typePartenaire&value='+this.value;">
			<OPTION value="0" <?php if($_SESSION['filtreTypePartenaire']=="0"){echo "selected";} ?>>Partenaires</OPTION>
			<?php 
			foreach ($lesTypePartenaires as $unTypePartenaire)
			{
			?>
				<option value="<?php echo $unTypePartenaire['idTypePartenaire'];?>" <?php if(isset($_SESSION['filtreTypePartenaire'])){if($_SESSION['filtreTypePartenaire']==$unTypePartenaire['idTypePartenaire']){echo "selected";}} ?>> <?php echo $unTypePartenaire['nomTypePartenaire']; ?></option>
			<?php
			}
			?>
		</select>
		</th>
		<th width="7%">
		<select name="saison_id" id='saison_id' onchange="javascript:location.href ='index.php?uc=affichage&action=affichageTableau&filtre=saison&value='+this.value;">
			<OPTION value="0" <?php if($_SESSION['filtreSaison']=="0"){echo "selected";} ?>>Saisons</OPTION>
			<?php 
			foreach ($lesSaisons as $uneSaison)
			{
			?>
				<option value="<?php echo $uneSaison['idSaison'];?>" <?php if(isset($_SESSION['filtreSaison'])){if($_SESSION['filtreSaison']==$uneSaison['idSaison']){echo "selected";}} ?>> <?php echo $uneSaison['nomSaison']; ?></option>
			<?php
			}
			?>
		</select>
		</th>
		<th>Contact</th>
		<th>fonction du contact</th>
		<th>Adresse</th>
		<th>Code Postal</th>
		<th>ville</th>
		<th>Telephone</th>
		<th width="5%">Mail</th>
		<th width="8%">Prestation</th>
		<th>Type de prestation</th>
		<th>Durée du contrat</th>
		<th width="6%">Remarques</th>
		<th>Montant promis</th>
		<th>Date du paiement</th>
		<th>Montant Payé</th>
		<th class="noExl">Modifier/<br>supprimer</th>
	</tr>
