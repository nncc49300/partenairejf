<div class="row">
		<h2>Copier un contrat</h2>
</div>
<form action="index.php?uc=gestionPartenaire&action=ajoutPartenaireScript" method="POST" class="form-ajout">
	<div class="row">
		<div class="col-sm-2">
			Section : 
		</div>
		<div class="col-sm-4">
			<select name="section_id" id='section_id'>
			<?php 
			foreach ($lesSections as $uneSection)
			{
			?>
				<option value="<?php echo $uneSection['idSection'];?>" <?php if($unPartenaire['section_id']==$uneSection['idSection']) echo "selected"; ?>> <?php echo $uneSection['nomSection']; ?></option>
			<?php
			}
			?>
		</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Raison sociale : 
		</div>
		<div class="col-sm-4">
			<select name="typePartenaire_id" id='typePartenaire_id'>
				
				<?php 
				foreach ($lesTypePartenaires as $unTypePartenaire)
				{
				?>
					<option value="<?php echo $unTypePartenaire['idTypePartenaire'];?>" <?php if($unPartenaire['typePartenaire_id']==$unTypePartenaire['idTypePartenaire']) echo "selected"; ?>> <?php echo $unTypePartenaire['nomTypePartenaire']; ?></option>
				<?php
				}
				?>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Nom du contact : 
		</div>
		<div class="col-sm-4">
			<select name="civiliteContact_id" id='civiliteContact_id'>
				<OPTION selected> </OPTION>
				<?php 
				foreach ($lesCivilites as $uneCivilite)
				{
				?>
					<option value="<?php echo $uneCivilite['idCiviliteContact'];?>"<?php if($unPartenaire['civiliteContact_id']==$uneCivilite['idCiviliteContact']) echo "selected"; ?>> <?php echo $uneCivilite['nomCiviliteContact']; ?></option>
				<?php
				}
				?>
			</select>
			<input type="text" name="nomContact" value="<?php echo $unPartenaire['nomContact']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Fonction du contact : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="fonctionContact" value="<?php echo $unPartenaire['fonctionContact']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Adresse : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="adresse" value="<?php echo $unPartenaire['adresse']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Code Postal : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="CP" value="<?php echo $unPartenaire['CP']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Ville : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="ville" value="<?php echo $unPartenaire['ville']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Téléphone : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="tel" value="<?php echo $unPartenaire['tel']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Mail : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="mail" value="<?php echo $unPartenaire['mail']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Prestation : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="prestation" value="<?php echo $unPartenaire['prestation']; ?>">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Type de prestation : 
		</div>
		<div class="col-sm-4">
			<select name="prestation_id" id='prestation_id'>
				<OPTION selected> </OPTION>
				<?php 
				foreach ($lesPrestations as $unePrestation)
				{
				?>
					<option value="<?php echo $unePrestation['idPrestation'];?>"<?php if($unPartenaire['prestation_id']==$unePrestation['idPrestation']) echo "selected"; ?>> <?php echo $unePrestation['nomPrestation']; ?></option>
				<?php
				}
				?>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Saison : 
		</div>
		<div class="col-sm-4">
			<select name="saison_id" id='saison_id'>
				<OPTION selected> </OPTION>
				<?php 
				foreach ($lesSaisons as $uneSaison)
				{
				?>
					<option value="<?php echo $uneSaison['idSaison'];?>"<?php if($unPartenaire['saison_id']+1==$uneSaison['idSaison']) echo "selected"; ?>> <?php echo $uneSaison['nomSaison']; ?></option>
				<?php
				}
				?>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Durée du contrat : 
		</div>
		<div class="col-sm-4">
			<select name="duree_id" id='duree_id' required>
				<OPTION selected> </OPTION>
				<?php 
				foreach ($lesDurees as $uneDuree)
				{
				?>
					<option value="<?php echo $uneDuree['idDuree'];?>"> <?php echo $uneDuree['nomDuree']; ?></option>
				<?php
				}
				?>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			Remarques : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="remarques" value="<?php echo $unPartenaire['remarques']; ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">
			Montant promis: 
		</div>
		<div class="col-sm-4">
			<input type="text" name="montantPromis">
		</div>
	</div>
	

	<div class="row">
		<div class="col-sm-2">
			Date du paiement: 
		</div>
		<div class="col-sm-4">
			<input type="date" name="datePaiement">
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-2">
			Montant Payé : 
		</div>
		<div class="col-sm-4">
			<input type="text" name="montant">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">
		</div>
		<div class="col-sm-4">
			<input type="submit" class="btn btn-success" value="Modifier">
		</div>
	</div>
</form>


