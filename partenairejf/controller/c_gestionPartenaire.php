<?php

$action = $_REQUEST['action'];

switch($action)
{
	case 'ajoutPartenaire':
	{
		$lesSections=$pdo->getLesSections();
		$lesCivilites=$pdo->getLesCivilites();
		$lesPrestations=$pdo->getLesPrestations();
		$lesDurees=$pdo->getLesDurees();
		$lesPartenaires = $pdo->getLesPartenaires();
		$lesTypePartenaires = $pdo->getLesTypePartenaires();
		$lesSaisons=$pdo->getLesSaisons();
		include("views/v_ajoutPartenaire.php");
		break;
	}

	case 'ajoutPartenaireScript':
	{
		$section_id=$_REQUEST['section_id'];
		$typePartenaire_id=$_REQUEST['typePartenaire_id'];
		$civiliteContact_id=$_REQUEST['civiliteContact_id'];
		$nomContact=$_REQUEST['nomContact'];
		$fonctionContact=$_REQUEST['fonctionContact'];
		$adresse=$_REQUEST['adresse'];
		$CP=$_REQUEST['CP'];
		$ville=$_REQUEST['ville'];
		$tel=$_REQUEST['tel'];
		$mail=$_REQUEST['mail'];
		$prestation=$_REQUEST['prestation'];
		$prestation_id=$_REQUEST['prestation_id'];
		$saison=$_REQUEST['saison_id'];
		$duree_id=$_REQUEST['duree_id'];
		$remarques=$_REQUEST['remarques'];
		$montant=$_REQUEST['montant'];
		$datePaiement=$_REQUEST['datePaiement'];
		$montantPromis=$_REQUEST['montantPromis'];

		$pdo->ajoutPartenaire($section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis);
		echo "Contrat bien enregistré. Vous allez être redirigé vers la page d'accueil.";
		
		header( "refresh:2;url=index.php" );
		break;
	}
	case 'modifierPartenaire':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$unPartenaire=$pdo->getUnPartenaire($idPartenaire);
		$lesSections=$pdo->getLesSections();
		$lesCivilites=$pdo->getLesCivilites();
		$lesPrestations=$pdo->getLesPrestations();
		$lesSaisons=$pdo->getLesSaisons();
		$lesDurees=$pdo->getLesDurees();
		$lesTypePartenaires = $pdo->getLesTypePartenaires();
		include("views/v_modifPartenaire.php");
		break;
	}

	case 'modifierPartenaireScript':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$section_id=$_REQUEST['section_id'];
		$typePartenaire_id=$_REQUEST['typePartenaire_id'];
		$civiliteContact_id=$_REQUEST['civiliteContact_id'];
		$nomContact=$_REQUEST['nomContact'];
		$fonctionContact=$_REQUEST['fonctionContact'];
		$adresse=$_REQUEST['adresse'];
		$CP=$_REQUEST['CP'];
		$ville=$_REQUEST['ville'];
		$tel=$_REQUEST['tel'];
		$mail=$_REQUEST['mail'];
		$prestation=$_REQUEST['prestation'];
		$prestation_id=$_REQUEST['prestation_id'];
		$saison=$_REQUEST['saison_id'];
		$duree_id=$_REQUEST['duree_id'];
		$remarques=$_REQUEST['remarques'];
		$montant=$_REQUEST['montant'];
		$datePaiement=$_REQUEST['datePaiement'];
		$montantPromis=$_REQUEST['montantPromis'];

		$pdo->modifierPartenaire($idPartenaire, $section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis);

		echo "Contrat bien modifié. Vous allez être redirigé vers la page d'accueil.";
		
		header( "refresh:3;url=index.php" );
		break;
	}

	case 'copiePartenaire':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$unPartenaire=$pdo->getUnPartenaire($idPartenaire);
		$lesSections=$pdo->getLesSections();
		$lesCivilites=$pdo->getLesCivilites();
		$lesPrestations=$pdo->getLesPrestations();
		$lesDurees=$pdo->getLesDurees();
		$lesPartenaires = $pdo->getLesPartenaires();
		$lesTypePartenaires = $pdo->getLesTypePartenaires();
		$lesSaisons=$pdo->getLesSaisons();

		include("views/v_copiePartenaire.php");
		break;
	}

	case 'supprimerPartenaire':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$pdo->supprimerPartenaire($idPartenaire);
		echo "Contrat bien supprimé. Vous allez être redirigé vers la page d'accueil.";
		header( "refresh:3;url=index.php" );
		break;
	}

	case 'ajoutTypePartenaire':
	{
		$lesTypePartenaires=$pdo->getLesTypePartenaires();
		include("views/v_ajoutTypePartenaire.php");
		break;
	}

	case 'ajoutTypePartenaireScript':
	{
		$nomTypePartenaire=$_REQUEST['nomTypePartenaire'];
		$pdo->ajoutTypePartenaire($nomTypePartenaire);
		echo "Partenaire bien ajouté. Vous allez être redirigé vers la page d'accueil.";
		header( "refresh:3;url=index.php?uc=gestionPartenaire&action=ajoutPartenaire" );
		break;
	}
	
	case 'ajoutSection':
	{
		$lesSections=$pdo->getLesSections();
		include("views/v_ajoutSection.php");
		break;
	}

	case 'ajoutSectionScript':
	{
		$nomSection=$_REQUEST['nomSection'];
		$pdo->ajoutSection($nomSection);
		echo "Section bien ajoutée. Vous allez être redirigé vers la page d'accueil.";
		header("refresh:3;url=index.php?uc=gestionPartenaire&action=ajoutPartenaire");
		break;
	}

}


?>


