﻿<?php
session_start();
require_once("util/fonctions.inc.php");
require_once("util/class.pdoPartenaireJf.inc.php");
include("views/v_entete.php") ;
$pdo = PdoPartenaireJf::getPdoPartenaireJf();

if(!isset($_SESSION['log']))
{
	?>
	<form action="index.php" method="POST">
	code d'accès : <input type="password" name="login"><br>
	<input type="submit" value="ok">
	</form>
	<?php
	if(isset($_POST['login']))
	{
		$resultat=$pdo->verifCode($_POST['login']);
		if (!$resultat)
		{
		    echo 'Code d\'accès erroné !';
		}
		else
		{
		    $_SESSION['log'] = $resultat['code'];
		    header('Location: index.php');
		}
	}
	exit;
} 

if(!isset($_REQUEST['uc'])){
    $uc = 'affichage';
 	$_SESSION['filtreSaison'] = 0;
	$_SESSION['filtreSection'] = 0;
	$_SESSION['filtreTypePartenaire'] = 0;
}
else
	$uc = $_REQUEST['uc'];
 
switch($uc)
{

	case 'affichage':
		{include("controller/c_affichage.php");break;}
	case 'gestionPartenaire':
		{include("controller/c_gestionPartenaire.php");break;}
	case 'deconnexion':
	{
		session_destroy();
		header('Location: index.php');
	}
	
}
include("views/v_pied.php") ;
?>

