-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: mysql-partenairejf.alwaysdata.net
-- Generation Time: Dec 07, 2018 at 04:52 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `partenairejf_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `civilitecontact`
--

CREATE TABLE `civilitecontact` (
  `idCiviliteContact` int(4) NOT NULL,
  `nomCiviliteContact` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `civilitecontact`
--

INSERT INTO `civilitecontact` (`idCiviliteContact`, `nomCiviliteContact`) VALUES
(1, 'M.'),
(2, 'Mme');

-- --------------------------------------------------------

--
-- Table structure for table `codeacces`
--

CREATE TABLE `codeacces` (
  `id` int(3) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codeacces`
--

INSERT INTO `codeacces` (`id`, `code`) VALUES
(1, 'JFsports49');

-- --------------------------------------------------------

--
-- Table structure for table `duree`
--

CREATE TABLE `duree` (
  `idDuree` int(4) NOT NULL,
  `nomDuree` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `duree`
--

INSERT INTO `duree` (`idDuree`, `nomDuree`) VALUES
(1, '1 an'),
(2, '2 ans'),
(3, '3 ans'),
(4, '4 ans');

-- --------------------------------------------------------

--
-- Table structure for table `partenaire`
--

CREATE TABLE `partenaire` (
  `idPartenaire` int(4) NOT NULL,
  `section_id` int(4) NOT NULL,
  `civiliteContact_id` int(4) NOT NULL,
  `nomContact` varchar(255) NOT NULL,
  `fonctionContact` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `CP` int(5) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `tel` int(10) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `prestation` varchar(255) NOT NULL,
  `saison_id` int(6) NOT NULL,
  `prestation_id` int(4) NOT NULL,
  `duree_id` int(4) NOT NULL,
  `remarques` text NOT NULL,
  `typePartenaire_id` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `datePaiement` date NOT NULL,
  `montantPromis` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partenaire`
--

INSERT INTO `partenaire` (`idPartenaire`, `section_id`, `civiliteContact_id`, `nomContact`, `fonctionContact`, `adresse`, `CP`, `ville`, `tel`, `mail`, `prestation`, `saison_id`, `prestation_id`, `duree_id`, `remarques`, `typePartenaire_id`, `montant`, `datePaiement`, `montantPromis`) VALUES
(1, 2, 1, 'Audouit Christian', 'Directeur d\'agence', '1 rue Joseph Foyer', 49360, 'Maulévrier', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 4, 500, '2015-09-09', 0),
(2, 2, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 130, 450, '2015-11-07', 0),
(3, 2, 2, 'Tridon', 'Directrice d\'agence', '55 rue du Commerce', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 6, 500, '2015-09-23', 0),
(4, 2, 1, 'Oger', 'Associé', '29 Avenue de la Tessoualle', 49300, 'Cholet', 0, '', 'Panneau 0.4mx0.6m', 3, 1, 3, '', 7, 200, '2015-10-20', 0),
(5, 2, 1, 'Pavageau Pascal', 'Associé', '2 rue Notre Dame de la Paix', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 8, 450, '2015-10-14', 0),
(7, 2, 1, 'Brunet Nicolas', 'Directeur de magasin', 'Bld Jacques Cassini', 49300, 'Cholet', 0, '', 'Panneau 2mx2m', 3, 1, 1, 'Contrat JF', 3, 600, '0000-00-00', 0),
(8, 2, 1, 'Vigneron', 'Directeur de magasin', '4 rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 3, 1, 3, 'Contrat JF', 10, 0, '0000-00-00', 0),
(9, 2, 1, 'Saudeau', 'Directeur', 'Les Audoins', 49280, 'Saint Léger Sous Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, 'Contrat JF', 11, 0, '0000-00-00', 0),
(10, 2, 2, 'Herault Annabelle', 'Directrice d\'agence', '83 Avenue Gambetta', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, 'Contrat JF', 12, 450, '0000-00-00', 0),
(11, 2, 1, 'Murzeau Jean-François', 'Associé', 'Bld du Cormier', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 13, 350, '2015-11-05', 0),
(14, 2, 1, 'Tison', 'Associé', 'Avenue de l\'Europe', 49300, 'Cholet', 0, '', 'Panneau 0.4mx0.6m', 3, 1, 3, '', 15, 200, '2016-01-08', 0),
(16, 2, 1, '', '', '11 rue Goerges Clémenceau', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, 'Contrat JF', 17, 300, '0000-00-00', 0),
(18, 2, 1, '', 'Directeur d\'agence', '11 Avenue Michelet', 49300, 'Cholet', 0, '', 'Panneau 1.5mx0.5m', 3, 1, 3, '', 18, 450, '2015-12-01', 0),
(19, 2, 1, 'Gimard', 'Gérant', 'Route du Puy St Bonnet', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 19, 450, '2015-09-25', 0),
(20, 2, 1, 'Brémont Philippe', 'Associé', '11 rue Georges Clémenceau', 49300, 'Cholet', 0, '', 'Panneau 1mx1m', 3, 1, 3, '', 20, 300, '2015-11-16', 0),
(21, 2, 1, 'Manceau', 'Responsable garage', '142 avenue du Maréchal Leclerc', 49300, 'Cholet', 0, 'garageduparadis@wanadoo.fr', 'Panneau 2mx1m', 3, 1, 3, '', 21, 500, '2016-01-14', 0),
(22, 2, 1, 'Brosset Florian', 'Commercial', 'Zi de la Coindrie', 49340, 'Trémentines', 0, '', 'Panneau 0.4mx0.6m', 3, 1, 3, '', 22, 450, '2015-12-16', 0),
(23, 2, 1, 'Touraine Jérome', 'Directeur', '5 rue Lavoisier', 49300, 'Cholet', 0, '', 'Jeu de maillots', 3, 1, 3, '', 24, 1200, '2015-11-13', 0),
(24, 2, 1, 'Joubert Thierry', 'Gérant', '5 rue des bons enfants', 49300, 'Cholet', 0, '', 'Jeu de maillots', 3, 1, 1, '', 25, 220, '2015-08-04', 0),
(25, 2, 1, '', '', '4 rue Charles Tellier', 49300, 'Cholet', 0, '', 'Jeu de maillots', 3, 1, 1, '', 26, 310, '2015-08-04', 0),
(26, 2, 1, 'Berçon Dominique', 'Directeur', '3 rue de l\'Artisanat', 85290, 'Mortagne sur Sèvre', 0, '', 'Jeu de maillots', 3, 1, 1, '', 27, 310, '2015-09-04', 0),
(27, 2, 1, 'Allaire Freddy', 'Directeur', 'Route d\'Yzernay ', 49360, 'LesCerqueux', 0, '', 'Jeux de maillots', 3, 1, 1, '', 28, 1800, '2015-10-02', 0),
(28, 2, 1, 'Dropsy Philippe', 'Directeur', '1 Avenue de l\'Europe', 49230, 'Saint Germain sur Moine', 0, '', 'Don sans contrepartie', 3, 1, 1, '', 29, 100, '2015-10-01', 0),
(29, 2, 1, 'Jallier et Tharreau', 'Gérant', 'Zi de la Blanchardière', 49300, 'Cholet', 0, '', 'Marquage au sol', 3, 1, 3, '', 30, 1000, '2015-10-06', 0),
(30, 2, 2, 'Bidault', 'Directrice d\'agence', '60 rue Saint Pierre', 49300, 'Cholet', 0, '', 'Chéquier JF', 3, 2, 1, 'Contrat JF', 31, 250, '0000-00-00', 0),
(31, 2, 1, 'Laudren', 'Directeur', '16 rue du Cédec', 89120, 'Charny', 0, '', 'Don sans contrepartie', 3, 1, 1, '', 32, 700, '2016-01-11', 0),
(32, 2, 1, 'Beaulieu Yannick', 'Responsable communication', '1 rue Gutenberg', 49300, 'Cholet', 0, '', 'Chéquier JF', 3, 2, 1, 'Contrat JF', 33, 400, '0000-00-00', 0),
(33, 2, 1, 'Raud Jean-François', 'Directeur', 'ZI du Parc', 49280, 'St Christophe du Bois', 0, '', 'Marquage au sol', 3, 1, 3, '', 34, 1000, '2015-10-23', 0),
(34, 2, 1, 'Guilleneuf Pascal', 'Directeur d\'agence', 'Le Haut Cormier', 44700, 'Orvault', 0, '', 'Encart programme de match', 3, 1, 1, '', 35, 300, '2016-01-08', 0),
(35, 2, 1, 'Lebas Benoit', 'Gérant', 'Zi du Cormier', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 36, 60, '2015-12-03', 0),
(36, 2, 2, '', '', 'Le rêve', 49690, 'Coron', 0, '', 'Encart programme de match', 3, 1, 1, '', 37, 60, '2016-01-25', 0),
(37, 2, 1, 'Tessier Stéphane', 'Gérant', '7 sqare de la Fontaine', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 38, 60, '2016-01-06', 0),
(38, 2, 1, 'Bequet', 'Patron', 'ZA des Landes', 49360, 'Toulemonde', 0, '', 'Encart programme de match', 3, 1, 1, '', 39, 60, '2015-11-13', 0),
(39, 2, 1, 'Barré Pierrick', 'Gérant', '45 rue de Rambourg', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 40, 60, '2015-11-24', 0),
(40, 2, 1, 'Raveleau Vincent', 'Gérant', '5 rue de Blois', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 41, 60, '2015-12-04', 60),
(41, 2, 1, 'Boitel Laurent', 'Directeur', 'ZA la Barboire', 85500, 'Chambretaud', 0, '', 'Encart programme de match', 3, 1, 1, 'Réduction sur facture', 42, 0, '0000-00-00', 60),
(42, 2, 1, 'Devienne Hervé', 'Directeur', 'Rue Nationale', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 43, 450, '2016-03-18', 0),
(43, 2, 1, 'Sourice Olivier', 'Directeur', '134 rue de la Girardière', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 44, 400, '2016-02-05', 0),
(44, 1, 1, 'Legendre', 'Directeur', '', 49300, 'Cholet', 241752467, 'inter.cholet@orange.fr', 'Encart carte adhérents + encart Brochure JF', 3, 2, 1, '', 45, 800, '0000-00-00', 0),
(45, 1, 1, 'Brunet', 'Directeur', 'Cassini Zac Des Cormiers, Rue Saint-Jacques', 49300, 'Cholet', 241715314, 'mickael.brunet@leroymerlin.fr', 'Encart carte adhérents + encart Brochure JF + prestation au magasin + partenaire journée ', 3, 2, 1, 'Vu avec Basket', 3, 1400, '0000-00-00', 0),
(46, 1, 1, 'Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 2, 'cholet@reseau-intersport.fr', '2 encarts dans brochure activités JF 15/16', 3, 2, 1, '', 46, 850, '0000-00-00', 0),
(47, 2, 1, 'Proux', 'PDG', '31 rue David d\'Anger', 49122, 'Le May sur Evre', 0, '', 'Panneau 2mx1m', 3, 1, 1, '', 47, 300, '2016-05-03', 0),
(48, 2, 1, 'Chambiron', 'Patron', '31 Avenue de la Libération', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 48, 60, '2016-04-28', 0),
(49, 2, 1, 'Granneau Patrice', 'Directeur', 'Rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 3, 1, 1, '', 49, 400, '2016-04-29', 0),
(50, 2, 1, 'Guyomarch', 'Gérant', '6 rue Bisson Bt B', 44100, 'Nantes', 0, '', 'Encart programme de match', 3, 1, 1, '', 50, 60, '2016-05-04', 0),
(52, 2, 1, 'Despré', 'Gérant', '82 rue Sadi Carnot', 49300, 'Cholet', 628821403, '', 'Encart programme de match', 3, 1, 1, '', 52, 60, '2016-05-06', 0),
(53, 2, 1, '', 'Associé', '37 Avenue de la Tessoualle', 49300, 'Cholet', 241565046, '', 'Panneau 2mx1m', 3, 1, 1, '', 53, 450, '2016-05-04', 0),
(54, 1, 2, 'Lumineau', 'Chargée de communication', '2 rue de la Baie d\'Hudson', 49300, 'Cholet', 241707414, '', 'Encart brochure + article newsletter JF', 3, 1, 1, 'Contrat signé pour 4 mois de mai à aout puis convention de 3 ans à partir de septembre', 54, 500, '0000-00-00', 0),
(55, 1, 1, 'Saudeau', 'Gérant', 'Les Audouins', 49280, 'St Léger sous Cholet', 241562430, 'saudeau.sarl@wanadoo.fr', 'Brochure JF + panneau SO1 + mise à dispo de salles + visibilité événements JF (foot + tennis)', 3, 1, 1, '', 55, 1800, '0000-00-00', 0),
(56, 1, 2, 'Bidault', 'Directrice', '60 rue Saint Pierre', 49300, 'Cholet', 241468213, 'virginie.bidault@mfam.fr', 'Chéquier JF', 3, 2, 1, 'Vu avec Basket', 31, 250, '0000-00-00', 0),
(57, 1, 2, 'Chauveau', 'Conseillère en financements', '16 boulevard Gustave Richard', 49300, 'Cholet', 241709110, 'delphine.chauveau@pretpartners.fr', 'Chéquier JF', 3, 2, 1, '', 56, 500, '0000-00-00', 0),
(58, 1, 1, 'Vigneron', 'Gérant', '4 rue de l\'arceau', 49300, 'Le Puy St Bonnet', 241564119, 'contact@groupe-sppec.fr', 'Brochure JF', 3, 2, 1, 'Vu avec Basket', 10, 500, '0000-00-00', 0),
(59, 1, 2, 'Lacourpaille', 'Gérante', '6 avenue Manceau', 49300, 'Cholet', 2, '', 'Chéquier JF', 3, 2, 1, '', 57, 500, '0000-00-00', 0),
(60, 1, 1, 'Moriceau', 'Gérant', 'BP 80527', 49305, 'Cholet cedex', 2, 'pierre-yves.moriceau@barr-heol.com', 'Brochure JF', 3, 2, 1, 'Vu avec Basket', 33, 600, '0000-00-00', 0),
(61, 1, 2, 'Riaud', 'Directrice marketing', '5 avenue Francis Bouet BP 90305', 49303, 'Cholet cedex', 2, 'annelaure.riaud@quadra.fr', 'Brochure JF', 3, 2, 1, '', 58, 250, '0000-00-00', 0),
(62, 1, 2, 'Gayraud', '', '27 rue de Mondement', 49300, 'Cholet', 2, 'AGayraud@mutuellelacholetaise.fr', 'Brochure JF + événements JF + chéquier JF + minibus', 3, 1, 3, 'Vu avec Basket + CMB', 17, 4200, '0000-00-00', 0),
(63, 3, 2, 'Gayraud', '', '27 rue de Mondement', 49300, 'Cholet', 241491600, 'AGayraud@mutuellelacholetaise.fr', 'Encart programme', 3, 1, 1, 'Contrat JF', 17, 500, '0000-00-00', 0),
(64, 1, 1, 'Martin', 'Directeur', '2 rue de la mairie', 79100, 'Vrines', 0, '', 'Don sans contrepartie', 3, 2, 1, '', 59, 200, '0000-00-00', 0),
(65, 2, 1, 'Jaumouille Franck', 'Conducteur de travaux', 'Rue du grand Pré', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 60, 600, '2016-05-09', 0),
(66, 2, 1, 'Romeo Gilles', 'Directeur', '10 sqare des grandes Claies', 49300, 'Cholet', 616092057, '', 'Panneau 2mx1m', 3, 1, 1, '', 61, 400, '2016-05-15', 0),
(67, 2, 1, 'Robert Stéphane', 'Directeur', '12 rue du grand Fief', 85580, 'St Michel en L\'herm', 0, '', 'Encart programme de match', 3, 1, 1, '', 62, 100, '2016-05-30', 0),
(68, 2, 1, 'Travers Sébastien', 'Directeur', '3 Avenue des calins', 49300, '', 0, '', 'Encart programme de match', 3, 1, 1, '', 63, 200, '2016-06-01', 0),
(69, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 64, 400, '2016-05-10', 0),
(70, 2, 1, 'Naud Gilbert', 'Directeur d\'agence', '1 Bld de la Victoire', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 1, 'Virement', 65, 450, '2016-05-24', 450),
(71, 2, 1, 'Moreau', 'Directeur', '126 Bld de Strasbourg', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 3, 'Virement', 66, 450, '2016-07-15', 450),
(72, 2, 1, 'Beton Yvan', 'Directeur', '3 rue du Patis', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 67, 60, '2016-05-25', 0),
(73, 2, 1, 'Baugé Franck', 'Directeur', '38 Avenue de Nantes', 49300, 'Cholet', 0, '', 'Achat lots', 3, 1, 1, '', 68, 100, '2016-05-25', 0),
(74, 2, 1, 'Rambaud Sébastien', 'Gérant', '27 rue Laënnec', 49300, 'Cholet', 0, '', 'Achat lots', 3, 1, 1, '', 69, 50, '2016-05-27', 0),
(75, 2, 1, 'Robineau Eric', 'Directeur', '14 rue Charles Messier', 49300, 'Cholet', 0, '', 'Achat lots', 3, 1, 1, '', 70, 75, '2016-05-26', 0),
(76, 2, 1, 'Laporte Patrice', 'Gérant', 'Rue Galilée ZA du Tacret', 85610, 'La Bernadière', 0, '', 'Encart programme de match', 3, 1, 1, '', 71, 300, '2016-06-08', 0),
(77, 2, 1, 'Barbarit Boris', 'Gérant', '58 rue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 73, 60, '2016-06-08', 60),
(78, 2, 1, 'Roussière', 'Responsable agence', '100 Avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 3, 1, 1, '', 74, 60, '2016-06-11', 0),
(79, 2, 1, 'Tisseau pierre', 'Directeur', '84 rue François de Chabot', 49360, 'Yzernay', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 78, 450, '2016-04-26', 450),
(80, 2, 2, 'Loup Pascale', 'Associée', '4 rue d\'Austerliz', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 3, 1, 3, '', 79, 450, '2016-04-26', 0),
(86, 2, 1, 'Allaire Freddy', 'Directeur', 'Route d\'Yzernay ', 49360, 'LesCerqueux', 241295400, '', 'Jeux de maillots', 4, 1, 1, '', 28, 1800, '2016-12-08', 1800),
(87, 2, 1, 'Racineux François', 'Co-dirigeant', '7 rue Sorel Tracy', 49300, 'Cholet', 241582522, '', 'Jeu de maillots', 4, 1, 1, '', 81, 672, '2016-08-30', 672),
(88, 2, 1, 'Savinaud Nicolas', 'Co-directeur', '49 Boulevard de la Rontardière', 49300, 'Cholet', 241300300, '', 'Jeu de sur maillots', 4, 1, 1, '', 82, 434, '2016-10-13', 435),
(89, 2, 1, 'Le Charles', 'Co-dirigeant', '7 rue Denis Papin', 49300, 'Cholet', 241622713, 'choletpoidslourds@wanadoo.fr', 'Panneau 2mx1m', 4, 1, 3, '', 83, 500, '2016-09-12', 500),
(90, 2, 1, 'Guerrier Patrice', 'Gérant', 'Avenue du Lac', 49300, 'Cholet', 241621395, '', 'Panneau 2mx1m', 4, 1, 3, '', 84, 500, '2016-06-27', 500),
(91, 2, 1, 'Jallier et Tharreau', 'Gérant', 'Zi de la Blanchardière', 49300, 'Cholet', 241622296, '', 'Marquage au sol', 4, 1, 1, '', 30, 1000, '2016-09-20', 1000),
(92, 2, 1, 'Frouin Henri', 'Directeur', '10 rue de Terre Neuve', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 85, 110, '2016-08-31', 110),
(93, 2, 1, 'Carré Jean-Christophe', 'Directeur', '90 Avenue des Baux', 13520, 'Maussane les Alpilles', 0, '', 'Sans contre partie', 4, 1, 1, '', 86, 500, '2016-08-18', 500),
(94, 2, 2, '', '', 'Le rêve', 49690, 'Coron', 0, '', 'Encart programme de match', 4, 3, 1, '', 37, 0, '0000-00-00', 70),
(95, 2, 1, 'Charbonneau Alain', 'Directeur', '44 avenue de la Marne', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 87, 75, '2016-09-05', 75),
(96, 2, 1, 'Belin Jean-Louis', 'Directeur', '2 avenue Léon Gambetta', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 88, 75, '2016-08-23', 75),
(97, 2, 1, 'Naud Gilbert', 'Directeur d\'agence', '1 Bld de la Victoire', 49300, 'Cholet', 0, '', 'Billeterie journée féminines', 4, 1, 1, 'Virement', 65, 400, '2016-10-11', 400),
(98, 2, 2, 'Dugast Chrystelle', '', '7 square d\'Amiens', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 89, 75, '2016-09-01', 75),
(99, 2, 2, 'Gabard Clarisse', 'Co gérante', '2 rue Beauregard', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 3, 1, '', 90, 0, '0000-00-00', 132),
(100, 2, 1, 'Bouvet Luc', 'Gérant', '13 rue du Bocage', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 91, 75, '2016-09-07', 75),
(101, 2, 1, 'Hugues Thomas', 'Directeur d\'agence', '36B avenue des Calins', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 92, 75, '2016-09-06', 75),
(102, 2, 1, 'Boitel Laurent', 'Directeur', 'ZA la Barboire', 85500, 'Chambretaud', 0, '', 'Encart programme de match', 4, 1, 1, '', 42, 75, '2016-11-15', 75),
(103, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 0, '', 'encart programme de match', 4, 1, 1, '', 64, 75, '2016-10-21', 75),
(104, 2, 1, 'Joubert Thierry', 'Gérant', '5 rue des bons enfants', 49300, 'Cholet', 0, '', 'Billeterie journée féminines', 4, 1, 1, '', 25, 250, '2016-10-11', 250),
(105, 2, 1, 'Guilleneuf Pascal', 'Directeur d\'agence', 'Le Haut Cormier', 44700, 'Orvault', 0, '', 'Encart programme de match', 4, 1, 1, '', 35, 300, '2016-10-11', 300),
(106, 2, 1, 'Tessier Stéphane', 'Gérant', '7 sqare de la Fontaine', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 38, 60, '2016-10-07', 60),
(107, 2, 1, 'Suaudeau Tony', 'Co gérant', 'ZI du Parc', 49280, 'St Christophe du Bois', 0, '', 'Encart programme de matc', 4, 1, 1, '', 93, 60, '2016-10-11', 60),
(110, 1, 1, 'Brunet', 'Directeur', 'Cassini Zac Des Cormiers, Rue Saint-Jacques', 49300, 'Cholet', 241715314, 'mickael.brunet@leroymerlin.fr', 'Partenaire matinées inscriptions JF + panneau SO1 2mx2m ', 4, 2, 1, 'Vu avec Basket', 3, 1000, '2016-09-08', 1000),
(111, 1, 1, 'Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', '3 encarts dans chéquier des partenaires JF', 4, 2, 1, '', 46, 1200, '2016-07-29', 1200),
(112, 1, 2, 'Lumineau', 'Chargée de communication', '2 rue de la Baie d\'Hudson', 49300, 'Cholet', 241707414, 'celine.biocoop-soleil.sud@orange.fr', 'Encart brochure JF + logo sur site internet + partenaire de 2 événements', 4, 1, 3, '', 54, 2100, '2017-06-21', 2100),
(113, 1, 2, 'Chauveau', 'Conseillère en financements', '16 boulevard Gustave Richard', 49300, 'Cholet', 241709110, 'delphine.chauveau@pretpartners.fr', 'Encart brochure JF + encart billetterie Danse', 4, 2, 1, 'Vu avec Danse', 56, 635, '2016-09-08', 635),
(114, 1, 2, 'BARON Olivia', 'Conseillère commerciale', 'Avenue P.P. Guilhem - BP 40252', 49072, 'Beaucouzé Cedex', 618107438, 'o.baron@abg.fr', 'Encart dans la brochure JF', 4, 2, 2, '', 95, 400, '2016-08-10', 400),
(115, 1, 1, 'DE GREEF Ulrich', 'Responsable', '66, rue de la gare', 85150, 'La Mothe-Achard', 676483305, 'naturorama@gmail.com', 'Encart dans le chéquier des partenaires JF', 4, 2, 1, '', 96, 0, '0000-00-00', 300),
(116, 1, 2, 'GODET Florence', 'Responsable régional', 'Rue Paul Henri Spaak', 49120, 'CHEMILLE', 241224970, 'f.godet@etrepure.fr', 'Encart dans le chéquier des partenaires JF + stand expo aux matinées inscriptions', 4, 2, 1, '', 97, 500, '2016-07-05', 500),
(117, 2, 1, 'Raud Jean-François', 'Directeur', 'ZI du Parc', 49280, 'St Christophe du Bois', 0, '', 'Marquage au sol', 4, 1, 1, '', 34, 1000, '2016-10-19', 1000),
(118, 2, 1, 'Leroux', 'Gérant', '17 rue Jacques Cathelineau', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 98, 60, '2016-10-20', 60),
(119, 8, 1, 'JAUFFRINEAU Ph', 'Président', 'ZA du Parc-Rue du parc', 49280, 'ST CHRISTOPHE DU BOIS', 2, 'pj@tsh-emballages.com', 'PJ', 4, 1, 1, '', 99, 500, '2016-10-07', 700),
(121, 8, 1, 'VIART Vincent', 'Directeur', '6 rue Kléber', 49300, 'CHOLET', 2, 'contact@cefw.org', 'LB', 4, 1, 1, '', 106, 150, '2016-10-10', 150),
(123, 8, 1, 'PINEAU Julien & Anthony', 'Gérant', 'ZA La gagnerie - St Georges des gardes', 49120, 'CHEMILLE en Anjou', 2, 'pineau.fils@free.fr', 'LB', 4, 1, 1, '', 113, 300, '2016-10-07', 300),
(124, 8, 1, 'MAURICE Dominique', 'Gérant', '28 Av Maudet', 49300, 'CHOLET', 2, 'contact@sima.immo', 'LB', 4, 1, 1, '', 101, 500, '2016-10-07', 500),
(125, 8, 1, 'NOYER Nicolas', '', 'ZI La  courbière', 49450, 'ST MACAIRE EN MAUGES', 2, 'nicolas.noyersa@orange.fr', 'LB', 4, 1, 1, '', 114, 100, '2016-10-11', 100),
(126, 8, 1, 'BOUCHET Jacky', 'Gérant', '10, ancienne rte de Trémentines', 49340, 'VEZINS', 2, 'bouchet.vezins@wanadoo.fr', 'LB', 4, 1, 1, '', 102, 400, '2016-10-05', 400),
(127, 8, 1, 'BEQUET Daniel', 'Gérant', 'ZA de la lande', 49360, 'TOUTLEMONDE', 2, 'sarl.bequet@orange.fr', 'LB', 4, 1, 1, '', 108, 200, '2016-10-26', 200),
(128, 8, 1, 'SENE Philippe', 'Gérant', 'ZA Les aubretieres- Avenue de l\'Europe', 49450, 'ST MACAIRE EN MAUGES', 2, 'contact@cciplarribeau.fr', 'LB', 4, 1, 1, '', 110, 200, '2016-10-19', 200),
(129, 8, 1, 'LANDREAU Christophe', 'Gérant', '77 bis rue de Bretagne', 49450, 'ST MACAIRE EN MAUGES', 2, 'sarl.landreau@orange.fr', 'LB', 4, 1, 1, '', 111, 200, '2016-10-12', 200),
(130, 8, 1, 'JANNIERE Philippe', 'Gérant', '251, rue Eugène Freyssinet', 85290, 'MORTAGNE/SEVRE', 2, 'contact@menuiserie-janniere.com', 'LB', 4, 1, 1, '', 112, 250, '2016-09-26', 250),
(131, 2, 1, 'Favreau Thierry', 'Gérant', '57 avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 116, 300, '2016-10-05', 300),
(132, 2, 1, 'Murzeau Jean-François', 'Associé', 'Bld du Cormier', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 13, 350, '2017-07-18', 350),
(133, 2, 1, 'Brémont Philippe', 'Associé', '11 rue Georges Clémenceau', 49300, 'Cholet', 0, '', 'Panneau 1mx1m', 4, 1, 3, '', 20, 300, '2016-12-01', 300),
(134, 2, 1, 'Merlet Bertrand', 'Directeur', '2 rue Notre Dame de la Paix', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 3, '', 8, 450, '2016-11-23', 450),
(135, 2, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 3, '', 130, 450, '2016-11-23', 450),
(136, 2, 1, 'Oger', 'Associé', '29 Avenue de la Tessoualle', 49300, 'Cholet', 0, '', 'Panneau 0.4mx0.6m', 4, 1, 3, '', 7, 200, '2016-12-08', 200),
(137, 2, 1, 'Gimard', 'Gérant', 'Route du Puy St Bonnet', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 19, 450, '2016-11-17', 450),
(138, 2, 2, 'Tridon', 'Directrice d\'agence', '55 rue du Commerce', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 3, '', 6, 500, '2016-12-08', 500),
(139, 2, 1, 'Audouit Christian', 'Directeur d\'agence', '1 rue Joseph Foyer', 49360, 'Maulévrier', 0, '', 'Panneau 2mx1m', 4, 1, 3, '', 4, 500, '2017-01-12', 500),
(140, 2, 1, '', 'Directeur d\'agence', '11 Avenue Michelet', 49300, 'Cholet', 0, '', 'Panneau 1.5mx0.5m', 4, 1, 3, '', 18, 450, '2016-12-16', 450),
(141, 2, 1, 'Tison', 'Associé', 'Avenue de l\'Europe', 49300, 'Cholet', 0, '', 'Panneau 0.4mx0.6m', 4, 1, 3, '', 15, 200, '2017-02-10', 200),
(142, 1, 2, 'Gayraud', 'Assistante de direction', '27 rue de Mondement', 49300, 'Cholet', 2, 'AGayraud@mutuellelacholetaise.fr', 'Brochure JF + événements JF + chéquier JF + minibus + autres', 4, 1, 3, 'Vu avec Basket + CMB', 17, 7000, '2017-02-17', 7000),
(143, 1, 1, 'Gilbert NAUD', 'Directeur', '1 bld de la Victoire', 49300, 'CHOLET', 2, 'gilbert.naud@creditmutuel.fr', 'Encart brochure JF', 4, 2, 1, '', 65, 500, '2016-06-22', 500),
(144, 1, 1, 'Legendre', 'Directeur', '', 49300, 'Cholet', 241752467, 'inter.cholet@orange.fr', 'Encart carte adhérents + encart Brochure JF', 4, 2, 1, '', 45, 800, '2016-07-08', 800),
(145, 1, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Encart brochure JF', 4, 2, 1, '', 130, 250, '2016-06-24', 250),
(146, 1, 2, 'Herault Annabelle', 'Directrice d\'agence', '83 Avenue Gambetta', 49300, 'Cholet', 0, '', '', 4, 1, 3, 'Vu avec Basket > 450', 12, 700, '2016-11-28', 700),
(147, 1, 2, 'VOUE', '', '8 bis, route de Nantes', 85290, 'Mortagne sur Sèvre', 2, 'sylvie.voue@mma.fr', '', 4, 1, 3, '', 117, 1500, '2017-06-02', 1500),
(148, 2, 1, 'Manceau', 'Responsable garage', '142 avenue du Maréchal Leclerc', 49300, 'Cholet', 0, 'garageduparadis@wanadoo.fr', 'Panneau 2mx1m', 4, 1, 3, '', 21, 500, '2016-12-27', 500),
(150, 2, 1, 'Laudren', 'Directeur', '16 rue du Cédec', 89120, 'Charny', 0, '', 'Don sans contrepartie', 4, 1, 1, '', 32, 700, '2017-02-20', 700),
(151, 2, 1, 'Dropsy Philippe', 'Directeur', '1 Avenue de l\'Europe', 49230, 'Saint Germain sur Moine', 0, '', 'Don sans contrepartie', 4, 1, 1, '', 29, 100, '2017-01-09', 100),
(152, 2, 1, 'Brunet Nicolas', 'Directeur de magasin', 'Bld Jacques Cassini', 49300, 'Cholet', 0, '', 'Panneau 2mx2m', 4, 1, 1, 'Contrat JF', 3, 500, '2017-01-05', 500),
(153, 2, 1, 'Raveleau Vincent', 'Gérant', '5 rue de Blois', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 41, 60, '2017-01-27', 60),
(154, 2, 1, 'Laporte Patrice', 'Gérant', 'Rue Galilée ZA du Tacret', 85610, 'La Bernadière', 0, '', 'Encart programme de match', 4, 1, 1, '', 71, 300, '2017-01-25', 300),
(155, 2, 1, 'Lebas Benoit', 'Gérant', 'Zi du Cormier', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 36, 60, '2017-01-18', 60),
(156, 2, 1, 'Vigneron', 'Directeur de magasin', '4 rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 10, 0, '0000-00-00', 450),
(157, 2, 2, 'Herault Annabelle', 'Directrice d\'agence', '83 Avenue Gambetta', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, 'Contrat JF', 12, 450, '2017-01-16', 450),
(158, 2, 1, '', '', '11 rue Goerges Clémenceau', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, 'Contrat JF', 17, 300, '2017-01-16', 300),
(159, 8, 1, 'MARY Jean-Louis', 'Gérant', 'Les landes Fleuries', 49600, 'ANDREZE', 2, 'bois.bocage.environnement@orange.fr', 'LB', 4, 1, 1, '', 125, 100, '2016-12-14', 100),
(160, 8, 1, 'LOKMANE Adil', 'Gérant', '19 rue Louis BLERIOT', 49300, 'CHOLET', 6, 'contact@iso-facades.com', 'LB', 4, 1, 1, '', 124, 300, '2016-12-13', 300),
(161, 8, 1, 'LANDAIS Philippe', 'Co-Gérant', '37 Avenue de la Libération', 49300, 'CHOLET', 2, 'contact@dmt-concept.fr', 'LB', 4, 1, 1, '', 126, 200, '2016-12-16', 200),
(162, 8, 1, 'LOISEAU Alain', 'Gérant', 'Rue des Tisserands', 49122, 'BEGROLLES en MAUGES', 2, 'sarl.loiseau.alain@laposte.net', 'LB', 4, 1, 1, '', 127, 300, '2016-12-16', 300),
(163, 8, 1, 'LAMEIRO Johnny', 'Gérant', '32, rue de la demoiselle', 85500, 'LES HERBIERS', 6, 'lepro.etanch@free.fr', 'LB', 4, 1, 1, '', 128, 400, '2017-01-25', 400),
(164, 8, 1, 'Florent PREZELIN', 'DG', '1, Bd des Sorinières', 49300, 'CHOLET', 2, '', 'PL', 3, 1, 3, 'Contrat signé sur saison 2015/2016. Panneau mis en place sur saison 2016/2017. 1er encaissement réalisé sur saison 2015/2016. 2016/2017 saison \"blanche\". prochain encaissement saison 2017/2018. contrat jusqu\'à fin de saison 2018/2019', 119, 499, '2016-06-14', 1097),
(165, 8, 1, 'Laurent CHENE', 'Gérant', 'la godinière', 49300, 'CHOLET', 2, 'lchene@bbox.fr', 'LC', 3, 1, 3, '', 120, 499, '2015-09-01', 1097),
(166, 8, 1, 'CESBRON', '', '14, rue Beauregard', 49300, 'CHOLET', 2, '', 'PL', 3, 1, 3, '', 121, 499, '2015-09-24', 1097),
(167, 8, 1, 'Romain FRADIN', 'Commercial', '', 0, '', 0, '', 'RF', 3, 1, 3, '', 122, 499, '2016-04-16', 1097),
(168, 8, 1, '', '', '', 49300, 'CHOLET', 0, '', 'Tournoi Jeunes 2016-2017', 4, 1, 1, 'Contact LC', 123, 150, '2017-01-15', 150),
(169, 8, 1, 'Matthieu GIBT', 'Gérant', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes 2016-2017', 4, 1, 1, 'Contact LC', 129, 50, '2017-01-15', 50),
(170, 8, 1, 'ALLANIC Gildas', 'Resp. Agence', '', 49300, 'CHOLET', 0, '', 'Compensation', 3, 3, 3, 'compliqué', 46, 798, '0000-00-00', 1097),
(171, 8, 1, 'BIDEAU Marwyn', 'Auto-Entrepeneur', '', 49300, 'CHOLET', 0, '', 'MB', 4, 2, 2, '', 130, 1427, '2016-12-15', 1429),
(172, 8, 1, 'JAUFFRINEAU Philippe', 'Président', '', 49280, 'ST CHRISTOPHE DU BOIS', 0, '', 'PJ', 4, 1, 3, 'Panneau BL', 99, 300, '2017-03-10', 900),
(173, 3, 1, 'Jean-Christophe CARRE', 'Dirigeant', '90, avenue des Baux', 13520, 'MAUSSANE LES ALPILLES', 0, '', 'Don sans contrepartie', 4, 1, 1, '', 86, 1500, '0000-00-00', 1500),
(174, 3, 2, 'Lucie MALINGE', 'Assistante marketing', 'Route d\'Yzernay', 49360, 'LES CERQUEUX', 2, 'lucie.malinge@pasquier.fr', 'Partenaire animations concours à 3 points et de dunks + encart programme + annonces micro + spots écran + remises de trophées', 4, 1, 1, '', 28, 2500, '0000-00-00', 2500),
(175, 3, 1, 'Gaëtan HUMEAU', 'Gérant', '5, rue du Grand Pré, ZI de l\'Ecuyère', 49300, 'CHOLET', 0, '', 'Encart programme', 4, 1, 1, '', 131, 500, '0000-00-00', 500),
(176, 3, 2, 'Céline COIFFARD', 'Responsable marketing', '5, rue de la Baie d\'Hudson', 49300, 'CHOLET', 2, 'c.coiffard@groupegca.com', 'Spots vidéo + annonces micro + panneaux LED + remise de récompenses', 4, 1, 1, '', 132, 1000, '0000-00-00', 1000),
(177, 3, 2, 'Claire PORTIER', 'Assistante de direction', 'Square Jean Bertin - parc d\'activités du Cormier - BP 80705', 49307, 'CHOLET Cedex', 2, 'claire.portier@geodis.com', 'Encart billetterie quotidienne + encart programme', 4, 2, 1, '', 133, 1300, '0000-00-00', 1300),
(178, 3, 1, 'Thomas DELAHAYE', 'Directeur général', 'Rue Edouard Branly, ZI La Bergerie', 49280, 'La Séguinière', 0, '', 'Partenaire défi du milieu de terrain + annonces micro + logo écrans', 4, 1, 1, '', 134, 750, '0000-00-00', 750),
(179, 3, 1, 'Benoît FORT', 'Directeur', 'ZA La Croix des Chaumes, 5 impasse des Landes Rousses', 85170, 'LE POIRE SUR VIE', 0, '', 'Encart programme', 4, 1, 1, '', 135, 500, '0000-00-00', 500),
(180, 3, 1, 'Rigaudeau', 'Dirigeant associé', '33, avenue de la Tessoualle, BP 31253', 49312, 'CHOLET Cedex', 0, 'f.jeanneau@geocjr.fr', 'Encart Pass Week-end + annonces micro + logos écrans + panneaux LED + remise de récompense', 4, 1, 1, '', 136, 1000, '0000-00-00', 1000),
(181, 8, 1, 'BABONNEAU Hervé', 'Gérant', '30, bd de la victoire', 49300, 'Cholet', 6, 'collectioncuisine@orange.fr', 'LB', 4, 1, 1, '', 141, 250, '2017-01-26', 250),
(182, 8, 1, 'CHARRIAT Cedric', 'Gérant', '29, Rue Pierre de Romans', 49360, 'Yzernay', 6, 'cepy49@orange.fr', 'LB', 4, 1, 1, '', 139, 130, '2017-01-27', 130),
(183, 8, 1, 'BIBARD Pierre', 'Gérant', '43, rue de la Jominière', 49300, 'Cholet', 6, 'scmc.bibard@orange.fr', 'LB', 4, 1, 1, '', 137, 100, '2017-01-16', 100),
(184, 8, 1, 'DEVEAUD Cyril', 'Gérant', '13, Rue de Fromenteau', 79700, 'St Pierre des Echaubrognes', 6, 'scdeveaud@orange.fr', 'LB', 4, 1, 1, '', 138, 200, '2017-01-26', 200),
(185, 8, 1, 'BREBION Romain', 'Gérant', '26 bis, rue du bocage', 49230, 'St Germain/Moine', 6, 'romainbrebion@free.fr', 'LB', 5, 1, 1, '', 140, 100, '2018-01-23', 100),
(186, 2, 1, 'Brosset Florian', 'Commercial', 'Zi de la Coindrie', 49340, 'Trémentines', 0, '', 'Panneau 0.4mx0.6m', 4, 1, 1, '', 22, 450, '2017-02-10', 450),
(187, 2, 1, 'Sourice Olivier', 'Directeur', '134 rue de la Girardière', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 44, 400, '2017-03-27', 400),
(188, 2, 1, 'Renou Patrick', 'Directeur d\'agence', '16 rue de Bonchamps', 49300, 'Cholet', 676770599, 'p.renou@financeconseil.fr', 'Panneau 0.4mx0.6m', 4, 1, 3, '', 142, 300, '2017-04-25', 300),
(189, 2, 1, 'Boitel Laurent', 'Directeur', 'ZA la Barboire', 85500, 'Chambretaud', 0, '', 'Encart programme de match', 4, 1, 1, '', 42, 100, '2017-04-25', 100),
(190, 2, 2, 'Cesbron Nathalie', 'Gérante', '61 rue de Lorraine', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 144, 50, '2017-05-02', 50),
(191, 2, 1, 'Proux', 'PDG', '31 rue David d\'Anger', 49122, 'Le May sur Evre', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 47, 300, '2017-05-04', 300),
(192, 8, 1, 'J.BRETEIN', 'Gérant', '2, rue de Sablé', 49300, 'CHOLET', 2, 'contact@espacedumaine.fr', 'Tournoi Jeunes 2016-2017', 4, 1, 1, '', 145, 300, '2017-01-05', 300),
(193, 2, 1, 'Tisseau pierre', 'Directeur', '84 rue François de Chabot', 49360, 'Yzernay', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 78, 450, '2017-05-12', 450),
(194, 2, 1, '', 'Associé', '37 Avenue de la Tessoualle', 49300, 'Cholet', 241565046, '', 'Panneau 2mx1m', 4, 1, 1, '', 53, 450, '2017-05-05', 450),
(195, 2, 1, 'Granneau Patrice', 'Directeur', 'Rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 49, 450, '2017-06-21', 450),
(196, 2, 2, 'Loup Pascale', 'Associée', '4 rue d\'Austerliz', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 79, 450, '2017-05-17', 450),
(197, 2, 1, 'Jaumouille Franck', 'Conducteur de travaux', 'Rue du grand Pré', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 60, 600, '2017-05-04', 600),
(198, 2, 1, 'Devienne Hervé', 'Directeur', 'Rue Nationale', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, '', 43, 450, '2017-05-26', 450),
(199, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 660049891, '', 'Panneau 2mx1m', 4, 1, 1, '', 64, 400, '2017-06-26', 400),
(200, 2, 1, 'Romeo Gilles', 'Directeur', '10 sqare des grandes Claies', 49300, 'Cholet', 616092057, '', 'Panneau 2mx1m', 4, 1, 1, '', 61, 400, '2017-05-05', 400),
(201, 8, 1, '', '', '', 49300, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 146, 100, '2017-01-05', 0),
(202, 8, 1, '', '', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 147, 50, '2017-01-05', 0),
(203, 8, 1, '', '', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 148, 150, '2017-01-05', 0),
(204, 8, 1, 'Christian AGUILLE', 'Gérant', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 149, 50, '2017-01-05', 0),
(205, 8, 1, 'Hervé MENEUST', 'Gérant', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 150, 50, '2017-01-05', 0),
(206, 8, 1, 'MERLET', '', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 151, 50, '2017-01-05', 0),
(207, 8, 1, '', '', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 152, 150, '2017-01-05', 0),
(208, 8, 1, '', '', '', 0, 'C', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 153, 50, '2017-01-05', 0),
(209, 8, 1, '', '', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 154, 100, '2017-01-05', 0),
(210, 8, 1, '', '', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 155, 50, '2017-01-05', 0),
(211, 8, 1, 'Patrice ROBICHON', 'Gérant', '', 0, 'CHOLET', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 156, 100, '2017-01-05', 0),
(212, 8, 1, 'Eric COULONNIER', 'Gérant', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 157, 100, '2017-01-05', 0),
(213, 8, 1, '', '', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 158, 50, '2017-01-05', 0),
(214, 8, 1, '', '', '', 0, '', 0, '', 'Tournoi Jeunes', 4, 1, 1, '', 159, 100, '2017-01-05', 0),
(215, 2, 1, 'Beton Yvan', 'Directeur', '3 rue du Patis', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 67, 60, '2017-05-17', 60),
(216, 2, 1, 'Robert Stéphane', 'Directeur', '12 rue du grand Fief', 85580, 'St Michel en L\'herm', 0, '', 'Encart programme de match', 4, 1, 1, '', 62, 100, '2017-05-22', 100),
(217, 2, 1, 'Roussière', 'Responsable agence', '100 Avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 74, 60, '2017-06-30', 60),
(218, 2, 1, 'Travers Sébastien', 'Directeur', '3 Avenue des calins', 49300, '', 0, '', 'Encart programme de match', 4, 1, 1, '', 63, 200, '2017-05-18', 200),
(219, 2, 1, 'Barbarit Boris', 'Gérant', '58 rue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 73, 60, '2017-05-19', 60),
(220, 1, 1, 'Moriceau', 'Gérant', '1 rue Gutenberg', 49300, 'Cholet', 0, 'pierre-yves.moriceau@barr-heol.com', 'Chéquier partenaires', 5, 2, 1, '', 33, 500, '2017-07-10', 500),
(221, 1, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', 'Chéquier partenaires', 5, 2, 1, '', 160, 500, '2017-05-05', 500),
(222, 3, 1, 'Laurent GOHE', 'Direction Technique et Développements ', '6 rue de Langeais', 49300, 'Cholet', 9, 'laurent@fingersstyle.com', 'Encart programme', 4, 1, 1, '', 161, 500, '0000-00-00', 500),
(223, 3, 2, 'Bénédicte RODRIGUEZ', 'Gérante associée', '48 avenue Georges Clémenceau', 49280, 'LA TESSOUALLE', 0, '', 'Don sans contrepartie', 4, 3, 1, '', 162, 0, '0000-00-00', 1635),
(224, 3, 1, 'Jean-Pierre PODGORSKI', 'Directeur général', 'PA de la Lande Saint Martin, 45 rue des Garottières', 44115, 'HAUTE-GOULAINE', 0, '', 'Don sans contrepartie', 4, 1, 1, '', 163, 1000, '0000-00-00', 1000),
(225, 3, 1, 'Jean-Yves PAPIN', 'Gérant', 'BP 23, 3 boulevard Jean Monnet', 49360, 'Maulévrier', 0, '', 'Partenaire officiel', 4, 1, 1, '', 164, 2750, '0000-00-00', 2750),
(226, 3, 1, 'Jacques BIRON', '', '37 route de la Tessoualle', 49300, 'Cholet', 0, '', 'Annonces micro/écrans + trophée Meilleur passeur', 4, 2, 1, '', 53, 200, '0000-00-00', 200),
(227, 1, 2, 'Céline Lumineau', 'Chargée de communication', '2 rue de la Baie d\'Hudson', 49300, 'Cholet', 241707414, 'celine.biocoop-soleil.sud@orange.fr', 'Encart brochure JF + logo sur site internet + partenaire de 2 événements', 5, 1, 2, '', 54, 2100, '0000-00-00', 2100),
(228, 1, 2, 'BARON Olivia', 'Conseillère commerciale', 'Avenue P.P. Guilhem - BP 40252', 49072, 'Beaucouzé Cedex', 618107438, 'o.baron@abg.fr', 'Encart brochure JF', 5, 2, 1, '', 95, 400, '0000-00-00', 400),
(229, 1, 1, 'Gilbert NAUD', 'Directeur', '1 bld de la Victoire', 49300, 'CHOLET', 2, 'gilbert.naud@creditmutuel.fr', 'Encart brochure JF', 5, 2, 1, '', 65, 500, '2017-06-29', 500),
(230, 1, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Encart brochure JF', 5, 2, 1, 'Manque TVA', 130, 500, '2017-04-28', 500),
(231, 1, 1, 'Philippe Legendre', 'Directeur', '', 49300, 'Cholet', 241752467, 'inter.cholet@orange.fr', 'Encart carte adhérents + encart Brochure JF', 5, 2, 1, '', 45, 800, '2017-07-28', 800),
(232, 1, 1, 'Jean-Cahrales Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Chéquier partenaires + brochure + boutique + newsletter JF', 5, 2, 1, 'Contrat JF pour autres sections : CMB, basket, tennis', 46, 2250, '2018-03-23', 2250),
(233, 3, 1, 'Jean-Cahrales Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Partenaire officiel', 5, 2, 1, 'Contrat global JF', 46, 2750, '2018-03-23', 2750),
(234, 1, 2, 'Chauveau', 'Conseillère en financements', '16 boulevard Gustave Richard', 49300, 'Cholet', 241709110, 'delphine.chauveau@pretpartners.fr', 'Encart brochure JF', 5, 2, 1, 'Vu avec Danse', 56, 500, '2017-07-20', 500),
(235, 15, 2, 'Chauveau', 'Conseillère en financements', '16 boulevard Gustave Richard', 49300, 'Cholet', 241709110, 'delphine.chauveau@pretpartners.fr', 'Encart billetterie Danse', 5, 2, 1, 'Contrat global JF', 56, 0, '0000-00-00', 140),
(236, 1, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 2, '', 'Brochure JF + événements JF + chéquier JF + minibus + autres', 5, 1, 2, 'Contrat global JF avec CMB, tennis et basket', 17, 5200, '2017-11-17', 5200),
(237, 3, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 2, '', 'Encart programme', 5, 1, 2, 'Contrat global JF', 17, 500, '2017-11-17', 500),
(238, 2, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 2, 'Contrat global JF', 17, 300, '2017-11-17', 300),
(239, 1, 2, 'VOUE', '', '8 bis, route de Nantes', 85290, 'Mortagne sur Sèvre', 2, 'sylvie.voue@mma.fr', 'Encart brochure JF', 5, 2, 2, '', 117, 500, '0000-00-00', 500),
(240, 3, 1, 'Bernard PAVAGEAU', 'Chargé de clients', 'La Novathèque - 5 boulevard Pierre Lecoq', 49300, 'Cholet', 0, '', 'Encart programme + trophée meilleur rebondeur', 4, 1, 1, '', 165, 600, '0000-00-00', 600),
(241, 3, 1, 'Olivier SOURICE', 'Directeur magasin', 'Route des Sables', 49300, 'Cholet', 0, '', 'Encart programme', 4, 1, 1, '', 44, 500, '0000-00-00', 500),
(242, 3, 1, 'Michel BASTAT', 'Gérant', '45, avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme', 4, 1, 1, '', 166, 250, '0000-00-00', 250),
(243, 3, 1, 'Jacky GEINDREAU', 'Gérant', '45, avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme', 4, 1, 1, '', 167, 250, '0000-00-00', 250),
(244, 3, 1, 'Karl GARNIER', 'Gérant', '3, rue des Forgerons', 44220, 'COUERON', 0, '', 'Encart programme', 4, 2, 1, '', 168, 500, '0000-00-00', 500),
(245, 3, 1, 'Frouin Henri', 'Directeur', '10 rue de Terre Neuve', 49300, 'Cholet', 0, '', 'Encart programme', 4, 1, 1, '', 85, 300, '0000-00-00', 300),
(246, 3, 1, 'Mickael Brunet', 'Directeur', 'Cassini Zac Des Cormiers, Rue Saint-Jacques', 49300, 'Cholet', 241715314, 'mickael.brunet@leroymerlin.fr', 'Panneaux LED, annonces micro/écrans, insertion flyers programme', 4, 2, 1, '', 3, 1000, '0000-00-00', 1000),
(247, 3, 1, 'Marc DELAYER', '', '24, rue de la Jominière CS21974', 49319, 'Cholet Cedex', 0, '', 'Encart programme + trophées 5 majeur', 4, 2, 1, '', 169, 1300, '0000-00-00', 1300),
(248, 3, 1, 'Vigneron', 'Gérant', '4 rue de l\'arceau', 49300, 'Le Puy St Bonnet', 241564119, 'contact@groupe-sppec.fr', 'Equipements son + image pour conférence de presse et événement', 4, 3, 1, '', 10, 0, '0000-00-00', 1744),
(249, 3, 1, 'Moriceau', 'Gérant', '1 rue Gutenberg', 49300, 'Cholet', 0, 'pierre-yves.moriceau@barr-heol.com', 'Annonces micro/écrans + panneaux LEDS', 4, 1, 1, '', 33, 1250, '0000-00-00', 1250),
(250, 3, 1, 'Moriceau', 'Gérant', '1 rue Gutenberg', 49300, 'Cholet', 0, 'pierre-yves.moriceau@barr-heol.com', 'Encart programme', 4, 2, 1, '', 33, 250, '0000-00-00', 250),
(251, 3, 1, 'Bruno AURIER', 'Directeur', '1, place des prairies', 49300, 'Cholet', 0, '', 'Encart programme + invitations partenaires', 4, 1, 1, '', 170, 800, '0000-00-00', 800),
(252, 3, 1, 'Gilbert NAUD', 'Directeur', '1 bld de la Victoire', 49300, 'CHOLET', 2, 'gilbert.naud@creditmutuel.fr', 'Partenaire caisse centrale + encart programme', 4, 2, 1, '', 65, 1250, '0000-00-00', 1250),
(253, 3, 1, 'Jean-Luc RAIMBAULT', '', 'Le Petit Cormier ', 49110, 'BEAUPREAU EN MAUGES', 241700455, '', 'Don de viandes', 4, 3, 1, '', 171, 0, '0000-00-00', 190),
(254, 3, 1, 'Sylvain BODET', 'Co-gérant', '72, rue du Général De Gaulle BP 1', 49340, 'TREMENTINES', 0, '', 'Prêt de matériel technique + écran', 4, 3, 1, 'Valorisation mécénat nature + compétence : 8140 €', 172, 0, '0000-00-00', 0),
(255, 3, 1, 'Thierry METHO GUEU', 'Gérant', '33, rue de Vrennes', 49600, 'LA CHAPPELLE DU GENET', 7, '', 'Prestation de sécurité et garidennage', 4, 3, 1, '', 173, 0, '0000-00-00', 912),
(256, 2, 1, 'Saudeau', 'Gérant', 'Les Audouins', 49280, 'St Léger sous Cholet', 241562430, 'saudeau.sarl@wanadoo.fr', 'Panneau 2mx1m', 5, 1, 3, '', 55, 500, '2017-05-17', 1500),
(257, 2, 1, 'Naud Gilbert', 'Directeur d\'agence', '1 Bld de la Victoire', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 4, 1, 1, 'Virement', 65, 450, '2017-05-04', 450),
(258, 2, 1, 'Rambaud Sébastien', 'Gérant', '27 rue Laënnec', 49300, 'Cholet', 0, '', 'Achat lots', 4, 1, 1, '', 69, 50, '2017-05-23', 50),
(259, 2, 1, 'Robineau Eric', 'Directeur', '14 rue Charles Messier', 49300, 'Cholet', 0, '', 'Achat lots', 4, 1, 1, '', 70, 75, '2017-05-05', 75),
(260, 2, 1, 'Baugé Franck', 'Directeur', '38 Avenue de Nantes', 49300, 'Cholet', 0, '', 'Achat lots', 4, 1, 1, '', 68, 100, '2017-05-09', 100),
(261, 1, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', '', 5, 3, 1, 'Valorisation montant prestations en fin de contrat', 160, 0, '0000-00-00', 0),
(262, 2, 1, 'Lebas Benoit', 'Gérant', 'Zi du Cormier', 49300, 'Cholet', 0, '', 'Marquage rond central ', 6, 1, 2, '', 36, 1200, '2018-10-08', 1200),
(263, 1, 2, 'GODET Florence', 'Responsable régional', 'Rue Paul Henri Spaak', 49120, 'CHEMILLE', 241224970, 'f.godet@etrepure.fr', 'Encart chéquier + présence événement', 5, 2, 1, '+ 500 pour Basket au féminin', 97, 600, '2017-07-31', 600),
(264, 3, 1, 'Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Partenaire officiel', 4, 2, 1, '', 46, 2500, '0000-00-00', 2500),
(265, 3, 1, 'Legendre', 'Directeur', '', 49300, 'Cholet', 241752467, 'inter.cholet@orange.fr', 'Partenaire officiel', 4, 2, 1, 'Dernière année du contrat de 3 ans, à renégocier pour 17-18', 45, 2500, '0000-00-00', 2500),
(266, 3, 2, 'Gabard Clarisse', 'Co gérante', '2 rue Beauregard', 49300, 'Cholet', 241585691, '', 'Impressions Badges, dossiers de presse, divers docs, tableaux des résultats, journal du mondial, panneaux', 4, 3, 1, '', 90, 0, '0000-00-00', 1312),
(267, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'achazelat@rgvgroupe.com', 'Panneaux LED + annonces micro', 4, 1, 1, '', 174, 800, '0000-00-00', 800),
(268, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'CHAZELAT Aurore <achazelat@rgvgroupe.com>', 'Panneaux LED + naming tournoi', 4, 1, 1, '', 82, 1200, '0000-00-00', 1200),
(269, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'CHAZELAT Aurore <achazelat@rgvgroupe.com>', 'Location locaux + prestation traiteur conférence de presse', 4, 3, 1, 'Valorisation montant prestations pas encore communiqué par Aurore - relance juin 2017', 82, 0, '0000-00-00', 0),
(270, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'achazelat@rgvgroupe.com', 'Concours de pronostics + annonces micros et écrans + distri flyers dans programme + distri flyers animations spéciales enfants avant CMB', 4, 2, 1, 'Relance règlement juin 2017', 175, 1480, '0000-00-00', 1480),
(271, 3, 2, 'Gayraud', '', '1 rue de la Sarthe', 49300, 'Cholet', 241491600, 'AGayraud@mutuellelacholetaise.fr', 'Encart programme', 4, 1, 2, 'Contrat JF', 17, 500, '2017-02-17', 500),
(272, 8, 1, 'Damien VIVION', 'Co-Gérant', '', 85000, 'La Roche/Yon', 0, '', 'Panneau BL', 5, 1, 1, 'Souhaite 1 investissement tourné vers les jeunes, le matériel et les bénévoles, à discrétion', 176, 5000, '2017-07-07', 5000),
(273, 8, 1, 'MURZEAU Jean-François', 'Co-Gérant', '', 49300, 'CHOLET', 0, '', 'Panneau BL', 5, 1, 3, 'Versement 3 fois contrat 3 ans', 13, 350, '2017-06-22', 1050),
(274, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 0, '', 'encart journée féminine', 5, 1, 1, '', 64, 75, '2017-09-25', 75),
(275, 2, 1, 'Carré Jean-Christophe', 'Directeur', '90 Avenue des Baux', 13520, 'Maussane les Alpilles', 0, '', 'Journée féminine', 5, 1, 1, 'Sans contre partie', 86, 500, '2017-07-20', 500),
(276, 2, 1, 'PREAU Daniel', 'Dirigeant', 'Rue de l\'Arceau', 49300, 'Cholet', 961692017, '', 'Encart double journée féminine', 5, 1, 1, '', 177, 110, '2017-07-05', 110),
(277, 2, 2, 'GODET Florence', 'Responsable régional', 'Rue Paul Henri Spaak', 49120, 'CHEMILLE', 241224970, 'f.godet@etrepure.fr', 'Présence événement Basket au féminin : distri échantillon + flyers + annonces micro + banderole', 5, 2, 1, 'Contrat global JF', 97, 500, '2018-04-18', 500),
(278, 2, 1, 'Jallier et Tharreau', 'Gérant', 'Zi de la Blanchardière', 49300, 'Cholet', 241622296, '', 'Marquage au sol', 5, 1, 3, '', 30, 1000, '2017-07-18', 3000),
(279, 2, 1, 'Charbonneau Alain', 'Directeur', '44 avenue de la Marne', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 87, 70, '2017-09-07', 70),
(281, 2, 1, 'Barré Pierrick', 'Gérant', '45 rue de Rambourg', 49300, 'Cholet', 0, '', 'Encart journée féminine', 5, 1, 1, '', 40, 50, '2017-07-26', 50),
(282, 2, 1, 'Hugues Thomas', 'Directeur d\'agence', '36B avenue des Calins', 49300, 'Cholet', 0, '', 'Encart journée féminine', 5, 1, 1, '', 92, 75, '2017-08-03', 75),
(283, 2, 1, 'Belin Jean-Louis', 'Directeur', '2 avenue Léon Gambetta', 49300, 'Cholet', 0, '', 'Encart journée féminine', 5, 1, 1, '', 88, 75, '2017-08-25', 75),
(284, 2, 1, 'Joubert Thierry', 'Gérant', '5 rue des bons enfants', 49300, 'Cholet', 0, '', 'Encart dans tous les programmes', 5, 1, 1, '', 25, 250, '2017-09-20', 250),
(285, 3, 2, 'Estelle FILLAUDEAU', 'Responsable marketing', '5, rue de la Baie d\'Hudson', 49300, 'CHOLET', 2, 'e.fillaudeau@groupegca.com', 'Billetterie + voiture', 5, 1, 1, '', 132, 1000, '0000-00-00', 1000),
(286, 2, 1, 'Allaire Freddy', 'Directeur', 'Route d\'Yzernay ', 49360, 'LesCerqueux', 241295400, '', 'Jeux de maillots', 5, 1, 1, '', 28, 1800, '2017-08-25', 1800),
(287, 2, 1, 'Frouin Henri', 'Directeur', '10 rue de Terre Neuve', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 85, 110, '2017-08-31', 110),
(288, 2, 2, '', '', 'Le rêve', 49690, 'Coron', 0, '', 'Encart journée féminine ', 5, 3, 1, '', 37, 0, '0000-00-00', 70),
(289, 2, 2, 'Gabard Clarisse', 'Co gérante', '2 rue Beauregard', 49300, 'Cholet', 0, '', 'Encart journée féminine ', 5, 3, 1, '', 90, 132, '0000-00-00', 132),
(290, 2, 1, 'Beraud Frédéric ', '', '5 rue de la baie d Hudson', 49300, 'Cholet', 0, '', 'Billetterie journée feminine', 5, 1, 1, '', 178, 650, '2017-08-31', 650),
(293, 2, 1, 'Vigneron', 'Directeur de magasin', '4 rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 10, 450, '2018-02-28', 450),
(294, 12, 1, 'Bruno AURIER', 'Directeur de site', '1 place Jean CHAVEL - CS70107', 49301, 'Cholet Cedex', 241495151, '', 'Panneau SO1 - 1,50 x 0,75 m', 5, 1, 1, '', 170, 0, '0000-00-00', 500),
(295, 8, 1, 'Laurent CHENE', 'Co-Gérant', '', 49300, 'CHOLET', 0, 'laurentchene72@gmail.com', 'Panneau BL', 5, 1, 2, 'Oubli en 2015-2016', 179, 299, '2017-08-16', 299),
(296, 8, 1, 'Romain FRADIN', '', '', 49300, 'CHOLET', 0, '', 'Panneau BL', 5, 1, 2, 'Année 2 Panneau BL', 122, 0, '0000-00-00', 299),
(297, 8, 1, 'CESBRON', 'gérant', '14, rue Beauregard', 49300, 'CHOLET', 2, '', 'Panneau BL', 5, 1, 2, 'Année 2 Panneau BL', 121, 0, '0000-00-00', 299),
(298, 8, 1, 'Florent PREZELIN', 'Directeur Général', '1, Bd Jean ROUYER', 49300, 'CHOLET', 2, '', 'Panneau BL', 5, 1, 2, 'Année 2 Panneau BL', 119, 0, '0000-00-00', 299),
(299, 8, 1, 'Laurent SOULARD', 'gérant', '7, rue de Langeais', 49300, 'CHOLET', 2, 'laurent.kiacholet@orange-business.fr', 'Textiles', 5, 1, 1, 'U15', 180, 700, '2017-09-27', 700),
(300, 8, 1, 'Cédric GRANGIEN', 'gérant', '22 rue de Metz', 49300, 'CHOLET', 6, '', 'Textiles', 5, 1, 1, '', 181, 520, '2017-09-19', 520),
(301, 8, 1, 'J.BRETEIN', '', '2, rue de Sablé', 49300, 'CHOLET', 2, 'contact@espacedumaine.fr', 'Textiles', 5, 1, 1, 'Maillots Seniors', 145, 700, '2017-10-10', 700),
(302, 8, 1, 'Benoit SOULARD', 'Gérant', '1, rue des salers', 49300, 'CHOLET', 2, '', 'Textiles', 5, 1, 1, 'U13', 182, 700, '2017-10-16', 500),
(303, 2, 1, 'Gimard', 'Gérant', 'Route du Puy St Bonnet', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 19, 450, '2017-11-17', 450),
(304, 2, 1, 'Le Charles', 'Co-dirigeant', '7 rue Denis Papin', 49300, 'Cholet', 241622713, 'choletpoidslourds@wanadoo.fr', 'Panneau 2mx1m', 5, 1, 3, '', 83, 500, '2017-11-04', 500),
(305, 2, 1, 'Moreau', 'Directeur', '126 Bld de Strasbourg', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 3, 'Virement', 66, 450, '2018-02-16', 450),
(306, 2, 1, 'Guerrier Patrice', 'Gérant', 'Avenue du Lac', 49300, 'Cholet', 241621395, '', 'Panneau 2mx1m', 5, 1, 3, '', 84, 500, '2017-10-14', 500),
(307, 8, 1, 'Daniel BEQUET', 'Gérant', 'ZA de la lande', 49360, 'TOUTLEMONDE', 2, 'sarl.bequet@orange.fr', 'Don Financier', 5, 1, 1, '', 108, 200, '2017-10-26', 200),
(308, 8, 1, 'LOISEAU Alain', 'Gérant', 'Rue des Tisserands', 49122, 'BEGROLLES en MAUGES', 2, 'sarl.loiseau.alain@laposte.net', 'Don Financier', 5, 1, 1, '', 127, 300, '2017-10-26', 300),
(309, 8, 1, 'PAVEC Jérôme', 'Co-Gérant', '26 RUE FRANCOIS BORDAIS', 49450, 'St MACAIRE EN MAUGES', 2, 'contact@akaze.fr', 'Don Financier', 5, 1, 1, 'Virement compte JF', 185, 500, '2017-11-13', 500),
(310, 8, 1, 'AUGEREAU Jean-Yves', 'Co-Gérant', 'LA PAPELLERIE', 49510, 'LA POITEVINIERE', 2, '', 'Don en nature (pommes)', 5, 1, 1, 'TJ 2018', 184, 50, '2018-01-27', 50),
(311, 8, 1, 'HERAUD Benoit', 'Gérant', '113 RUE DE LORRAINE', 49300, 'CHOLET', 2, '', 'Don Financier', 5, 1, 1, 'TJ 2018', 183, 0, '0000-00-00', 50),
(312, 8, 1, 'JAUFRINEAU Philippe', 'Président', 'ZA du parc III, 35, rue du parc', 49280, 'ST CHRISTOPHE DU BOIS', 2, 'pj@tsh-emballages.com', 'Textiles', 5, 1, 1, 'Maillots Seniors', 99, 700, '2017-11-03', 600);
INSERT INTO `partenaire` (`idPartenaire`, `section_id`, `civiliteContact_id`, `nomContact`, `fonctionContact`, `adresse`, `CP`, `ville`, `tel`, `mail`, `prestation`, `saison_id`, `prestation_id`, `duree_id`, `remarques`, `typePartenaire_id`, `montant`, `datePaiement`, `montantPromis`) VALUES
(313, 8, 1, 'Philippe RENOU', 'Co-Gérant', '11 rue Madeleine', 49510, 'LA POITEVINIERE', 2, '', 'Don Financier', 5, 1, 1, 'TJ 2018', 186, 50, '2018-01-30', 50),
(314, 8, 1, 'MAURICE Dominique', 'Gérant', '28 Av Maudet', 49300, 'CHOLET', 2, 'contact@sima.immo', 'LB', 5, 1, 1, '', 101, 500, '2017-11-09', 500),
(315, 8, 1, 'GRANDIN', 'Gérant', '11, Bd Delhumeau Plessis', 49300, 'CHOLET', 2, 'cholet@fonteneau-immobilier.com', 'Don Financier', 5, 1, 1, '', 187, 100, '2017-11-09', 100),
(316, 2, 1, 'Coumoul Christophe ', 'Gerant', '47 rue des Bons Enfants ', 49300, 'Cholet', 665518607, 'auxdixecus@orange.fr', 'Panneau  2m × 1m', 5, 1, 3, '', 188, 200, '2018-01-03', 200),
(317, 2, 1, 'Raud Jean-François', 'Directeur', 'ZI du Parc', 49280, 'St Christophe du Bois', 0, '', 'Marquage au sol', 5, 1, 3, '', 34, 500, '2017-11-25', 1000),
(318, 8, 1, 'PELISSIER Sylvain', 'Gérant', 'ZA de la gagnerie', 49120, 'CHEMILLE', 2, 'contact@pelissiermaconnerie.fr', 'Don Financier', 5, 1, 1, '', 189, 100, '2017-11-20', 100),
(319, 8, 1, 'PINEAU Julien & Anthony', 'Gérant', 'ZA La gagnerie - St Georges des gardes', 49120, 'CHEMILLE en Anjou', 2, 'pineau.fils@free.fr', 'Don Financier', 5, 1, 1, '', 113, 300, '2017-11-23', 300),
(320, 8, 1, 'CHARRIAT Cedric', 'Gérant', '29, Rue Pierre de Romans', 49360, 'Yzernay', 6, 'cepy49@orange.fr', 'Don Financier', 5, 1, 1, '', 139, 130, '2017-11-20', 100),
(321, 8, 1, 'BOUCHET Jacky', 'Gérant', '10, ancienne rte de Trémentines', 49340, 'VEZINS', 2, 'bouchet.vezins@wanadoo.fr', 'Don Financier', 5, 1, 1, '', 102, 400, '2017-11-22', 400),
(322, 8, 1, 'BIBARD Pierre', 'Gérant', '43, rue de la Jominière', 49300, 'Cholet', 6, 'scmc.bibard@orange.fr', 'Don Financier', 5, 1, 1, '', 137, 100, '2017-11-10', 100),
(323, 8, 1, 'LANDREAU Christophe', 'Gérant', '77 bis rue de Bretagne', 49450, 'ST MACAIRE EN MAUGES', 2, 'sarl.landreau@orange.fr', 'LB', 5, 1, 1, '', 111, 200, '2017-12-08', 200),
(324, 8, 1, 'LOKMANE Adil', 'Gérant', '19 rue Louis BLERIOT', 49300, 'CHOLET', 6, 'contact@iso-facades.com', 'LB', 5, 1, 1, '', 124, 300, '2017-12-08', 300),
(325, 8, 1, 'Frédéric GERFAULT', 'Gérant', 'ZA des Rosiers', 49120, 'NEUVY EN MAUGES', 2, 'ogerfreresneuvy@wanadoo.fr', 'Don Financier', 5, 1, 1, '', 190, 200, '2017-12-08', 200),
(327, 2, 1, 'Guilleneuf Pascal', 'Directeur d\'agence', '31 rue du nouveau Bêle', 44470, 'CARQUEFOU', 612279238, '', 'Encart programme de match', 5, 1, 1, '', 35, 300, '2017-12-01', 300),
(328, 2, 1, 'Chambiron', 'Patron', '31 Avenue de la Libération', 49300, 'Cholet', 0, '', 'Encart programme de match', 4, 1, 1, '', 48, 60, '2017-12-07', 60),
(329, 2, 1, 'Suaudeau Tony', 'Co gérant', 'ZI du Parc', 49280, 'St Christophe du Bois', 0, '', 'Encart programme de matc', 5, 1, 1, '', 93, 60, '2017-12-15', 60),
(330, 2, 1, 'Merlet Bertrand', 'Directeur', '2 rue Notre Dame de la Paix', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 3, '', 8, 450, '2018-01-04', 450),
(331, 2, 1, 'Brémont Philippe', 'Associé', '11 rue Georges Clémenceau', 49300, 'Cholet', 0, '', 'Panneau 1mx1m', 5, 1, 1, '', 20, 300, '2018-01-15', 300),
(332, 1, 2, 'Herault Annabelle', 'Directrice d\'agence', '83 Avenue Gambetta', 49300, 'Cholet', 0, '', '', 5, 1, 1, 'Vu avec Basket > 450', 12, 700, '2017-10-27', 700),
(333, 3, 2, 'Claire PORTIER', 'Assistante de direction', 'Square Jean Bertin - parc d\'activités du Cormier - BP 80705', 49307, 'CHOLET Cedex', 2, 'claire.portier@geodis.com', 'Encart billetterie quotidienne + encart programme', 5, 2, 1, '', 133, 1300, '0000-00-00', 1300),
(334, 3, 1, 'Moriceau', 'Gérant', '1 rue Gutenberg', 49300, 'Cholet', 0, 'pierre-yves.moriceau@barr-heol.com', 'Annonces micro/écrans + panneaux LEDS', 5, 1, 1, 'Les arches du Chouan', 33, 1250, '0000-00-00', 1250),
(335, 3, 1, 'Moriceau', 'Gérant', '1 rue Gutenberg', 49300, 'Cholet', 0, 'pierre-yves.moriceau@barr-heol.com', 'Encart programme', 5, 2, 1, 'Genel SAS', 33, 250, '0000-00-00', 250),
(336, 3, 2, 'Lucie MALINGE', 'Assistante marketing', 'Route d\'Yzernay', 49360, 'LES CERQUEUX', 2, 'lucie.malinge@pasquier.fr', 'Partenaire animations concours à 3 points et de dunks + encart programme + annonces micro + spots écran + remises de trophées', 5, 1, 1, '', 28, 2500, '2018-01-04', 2500),
(337, 3, 1, 'Jean-Christophe CARRE', 'Dirigeant', '90, avenue des Baux', 13520, 'MAUSSANE LES ALPILLES', 0, '', 'Don sans contrepartie', 5, 1, 1, '', 86, 1500, '0000-00-00', 1500),
(338, 3, 1, 'Sylvain BODET', 'Co-gérant', '72, rue du Général De Gaulle BP 1', 49340, 'TREMENTINES', 0, '', 'Prêt de matériel technique + écran', 5, 3, 1, 'Valorisation mécénat nature + compétence : 8140 €', 172, 0, '0000-00-00', 0),
(339, 3, 1, 'PAVAGEAU', '', '', 0, '', 0, '', 'Tour de cou + encart programme offert', 4, 2, 1, '', 192, 500, '0000-00-00', 500),
(340, 3, 2, 'Aurélie Giraud', 'Responsable des ressources humaines', ' 37 rue Pierre et Marie Curie - BP 10966', 49309, 'CHOLET Cedex', 0, '', 'Don sans contrepartie (encart programme offert)', 4, 1, 1, '', 193, 900, '0000-00-00', 900),
(341, 3, 1, 'Jean-François MURZEAU', 'Gérant', 'Boulevrad du Cormier', 49300, 'CHOLET', 2, 'jfm@graphic49.com', '', 4, 2, 1, '', 13, 375, '0000-00-00', 375),
(342, 2, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 130, 450, '2018-01-15', 450),
(344, 2, 1, '', 'Directeur d\'agence', '11 Avenue Michelet', 49300, 'Cholet', 0, '', 'Panneau 1.5mx0.5m', 5, 1, 1, '', 18, 450, '2018-01-11', 450),
(345, 8, 1, 'Fabien Clémot', 'Gérant', 'Av de la libération', 49300, 'CHOLET', 0, '', 'Don Financier', 5, 1, 1, 'Pas de communication', 191, 150, '2018-01-05', 0),
(346, 2, 2, 'Herault Annabelle', 'Directrice d\'agence', '83 Avenue Gambetta', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, 'Contrat JF', 12, 450, '2017-10-27', 450),
(348, 2, 1, 'Laporte Patrice', 'Gérant', 'Rue Galilée ZA du Tacret', 85610, 'La Bernadière', 0, '', 'Encart programme de match', 5, 1, 1, '', 71, 200, '2018-01-20', 300),
(349, 8, 1, 'Gaëtan', 'Gérant', '13, Bd Delhumeau Plessis', 49300, 'CHOLET', 0, '', 'Don Financier', 5, 1, 1, 'TJ 2018', 194, 50, '2018-01-27', 50),
(350, 8, 1, 'JANNIERE Philippe', 'Gérant', '251, rue Eugène Freyssinet', 85290, 'MORTAGNE/SEVRE', 2, 'contact@menuiserie-janniere.com', 'LB', 5, 1, 1, '', 112, 200, '2018-01-23', 250),
(351, 8, 1, 'MARY Jean-Louis', 'Gérant', 'Les landes Fleuries', 49600, 'ANDREZE', 2, 'bois.bocage.environnement@orange.fr', 'LB', 5, 1, 1, '', 125, 100, '2018-01-23', 100),
(352, 8, 1, 'Richard AUDUSSEAU', 'Gérant', '', 0, 'St MACAIRE EN MAUGES', 0, '', 'Don Financier', 5, 1, 1, 'TJ 2018', 195, 0, '0000-00-00', 50),
(353, 12, 1, 'HERRAULT', '', '1 Place Michel Ange', 49300, 'CHOLET', 2, '', 'Panneau SO1', 5, 1, 1, '', 196, 500, '0000-00-00', 500),
(354, 12, 1, 'Romain GERMOND', 'Gérant', '2 avenue Francis Bouet', 49300, 'CHOLET', 8, 'romain.germond@mma.fr', 'Panneau SO1', 5, 1, 1, '', 197, 1500, '2017-12-08', 1500),
(355, 2, 1, 'Raveleau Vincent', 'Gérant', '5 rue de Blois', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 41, 60, '2018-02-08', 60),
(356, 2, 1, 'Manceau', 'Responsable garage', '142 avenue du Maréchal Leclerc', 49300, 'Cholet', 0, 'garageduparadis@wanadoo.fr', 'Panneau 2mx1m', 5, 1, 3, '', 21, 500, '2018-02-05', 500),
(357, 8, 1, 'Mickael JEAN', 'Gérant', '', 49300, 'CHOLET', 0, '', 'Don en nature(textile)', 5, 3, 1, 'traitement du dossier par la bande. sans passer par notre protocole Foot', 198, 110, '2018-01-23', 110),
(358, 8, 1, 'Abdelkrib Kassouat', 'Gérant', '5, rue du devau', 49300, 'CHOLET', 0, 'a.ka.thermique@gmail.com', 'Don en nature(textile)', 5, 3, 1, 'traitement du dossier par la bande. sans passer par notre protocole Foot', 199, 220, '2018-01-23', 220),
(359, 8, 1, 'Eric LENORMAND', 'Directeur de site', '', 49300, 'CHOLET', 0, 'eric_lenormand@carrefour.com', 'Don en nature(textile & matériel)', 5, 3, 1, 'traitement du dossier par la bande. sans passer par notre protocole Foot', 200, 200, '2018-01-23', 200),
(360, 8, 1, 'Philippe LANDAIS', 'Gérant', '37 Avenue de la Libération', 49300, 'CHOLET', 0, 'contact@dmt-concept.fr', 'Don Financier', 5, 1, 1, '', 126, 250, '2018-02-13', 200),
(361, 8, 1, 'JAUFRINEAU Philippe', 'Président', 'ZA du parc III, 35, rue du parc', 49280, 'ST CHRISTOPHE DU BOIS', 0, 'pj@tsh-emballages.com', 'Contrat Panneau', 5, 1, 3, 'Année 2 Panneau BL', 99, 300, '2018-02-20', 300),
(362, 8, 1, 'BREBION Romain', 'Gérant', '26 bis, rue du bocage', 49230, 'St Germain/Moine', 6, 'romainbrebion@free.fr', 'LB', 4, 1, 1, '', 140, 100, '2017-01-23', 100),
(363, 2, 1, 'Jobard Alain', 'Gérant', '2 rue Jean Monnet', 85130, 'La Verrie', 241659595, '', 'Jeu de maillots', 5, 1, 1, '', 201, 500, '2018-01-31', 500),
(364, 8, 1, 'SENE Philippe', 'Gérant', 'ZA Les aubretieres- Avenue de l\'Europe', 49450, 'ST MACAIRE EN MAUGES', 2, 'contact@cciplarribeau.fr', 'LB', 5, 1, 1, '', 110, 100, '2018-02-20', 100),
(365, 2, 1, 'Favreau Thierry', 'Gérant', '57 avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 116, 300, '2018-03-01', 300),
(366, 2, 1, 'Rousseau', 'Josselin', '', 49300, 'Cholet', 659418349, '', 'Encart dans programme de match', 5, 1, 1, '', 202, 40, '2018-03-10', 40),
(368, 2, 1, 'Brosset Florian', 'Commercial', 'Zi de la Coindrie', 49340, 'Trémentines', 0, '', 'Panneau 0.4mx0.6m', 5, 1, 1, '', 22, 450, '2018-03-15', 450),
(369, 2, 2, 'Tridon', 'Directrice d\'agence', '55 rue du Commerce', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 6, 500, '2018-03-18', 500),
(371, 2, 1, 'Devienne Hervé', 'Directeur', 'Rue Nationale', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 3, '', 43, 450, '2017-05-24', 450),
(372, 2, 1, 'Chambiron', 'Patron', '31 Avenue de la Libération', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 48, 60, '2017-12-07', 60),
(373, 2, 1, 'Rousseau', 'Gérant', '', 49300, 'Cholet', 659418349, '', 'Encart dans programme de match', 5, 1, 1, '', 202, 60, '2018-01-26', 60),
(374, 2, 1, 'Rousseau', 'Gérant', '', 49300, 'Cholet', 659418349, '', 'Panneau 2m x 1m', 5, 1, 3, '', 202, 250, '2018-03-10', 500),
(375, 2, 1, 'Rambaud Sébastien', 'Gérant', '27 rue Laënnec', 49300, 'Cholet', 0, '', 'Achat lots', 5, 1, 1, '', 69, 60, '2018-03-18', 60),
(376, 2, 1, 'Tison', 'Associé', 'Avenue de l\'Europe', 49300, 'Cholet', 0, '', 'Panneau 0.4mx0.6m', 5, 1, 3, '', 15, 200, '2018-04-19', 200),
(377, 2, 1, 'Robineau Eric', 'Directeur', '14 rue Charles Messier', 49300, 'Cholet', 0, '', 'Achat lots', 5, 1, 1, '', 70, 200, '0000-00-00', 200),
(378, 2, 1, 'Baugé Franck', 'Directeur', '38 Avenue de Nantes', 49300, 'Cholet', 0, '', 'Achat lots', 5, 1, 1, '', 68, 100, '0000-00-00', 100),
(379, 2, 1, 'Laudren', 'Directeur', '16 rue du Cédec', 89120, 'Charny', 0, '', 'Don sans contrepartie', 5, 1, 1, '', 32, 500, '2018-04-18', 500),
(380, 2, 1, 'Sourice Olivier', 'Directeur', '134 rue de la Girardière', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 44, 450, '0000-00-00', 450),
(381, 2, 1, 'Proux', 'PDG', '31 rue David d\'Anger', 49122, 'Le May sur Evre', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 47, 300, '2018-05-02', 300),
(382, 2, 1, 'Barbarit Boris', 'Gérant', '58 rue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 73, 60, '2018-05-05', 60),
(383, 2, 1, 'Gohé Laurent', 'Gérant', '6 rue Langeais', 49300, 'Cholet', 631179209, 'laurent@fingersstyle.com', 'Panneau 0.4m x 0.6m', 5, 1, 3, '', 204, 250, '2018-05-04', 250),
(384, 2, 1, 'Robert Stéphane', 'Directeur', '12 rue du grand Fief', 85580, 'St Michel en L\'herm', 0, '', 'Encart programme de match', 5, 1, 1, '', 62, 100, '2018-05-14', 100),
(385, 2, 1, 'Renou Patrick', 'Directeur d\'agence', '16 rue de Bonchamps', 49300, 'Cholet', 676770599, 'p.renou@financeconseil.fr', 'Panneau 0.4mx0.6m', 5, 1, 3, '', 142, 300, '2018-05-14', 300),
(386, 2, 1, 'Travers Sébastien', 'Directeur', '3 Avenue des calins', 49300, '', 0, '', 'Encart programme de match', 5, 1, 1, '', 63, 200, '2018-05-09', 200),
(387, 2, 1, 'Beton Yvan', 'Directeur', '3 rue du Patis', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 67, 60, '2018-05-24', 60),
(388, 2, 1, 'Roussière', 'Responsable agence', '100 Avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 74, 60, '2018-06-26', 60),
(389, 2, 1, 'Castin Michel', 'Gérant', '9 rue Eugène Brémond', 49300, 'Cholet', 0, '', 'Panneau 1.5mx0.75m', 5, 1, 3, '', 205, 500, '2018-05-30', 500),
(390, 2, 2, 'Bidault', 'Directrice d\'agence', '54 rue Georges Clémenceau', 49300, 'Cholet', 0, '', '3 jeux de maillots jeunes', 5, 2, 1, '', 31, 1950, '2018-07-27', 1950),
(391, 2, 1, 'Romeo Gilles', 'Directeur', '10 sqare des grandes Claies', 49300, 'Cholet', 616092057, '', 'Panneau 2mx1m', 5, 1, 1, '', 61, 400, '2018-06-22', 400),
(393, 2, 1, 'Murzeau Jean-François', 'Associé', 'Bld du Cormier', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 13, 350, '2018-07-12', 350),
(394, 2, 1, 'Naud Gilbert', 'Directeur d\'agence', '1 Bld de la Victoire', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, 'Virement', 65, 450, '2018-07-17', 450),
(395, 2, 1, '', 'Associé', '37 Avenue de la Tessoualle', 49300, 'Cholet', 241565046, '', 'Panneau 2mx1m', 5, 1, 1, '', 53, 0, '0000-00-00', 450),
(396, 2, 1, 'Jaumouille Franck', 'Conducteur de travaux', 'Rue du grand Pré', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 60, 600, '2018-07-17', 600),
(397, 2, 1, 'Tisseau pierre', 'Directeur', '84 rue François de Chabot', 49360, 'Yzernay', 0, '', 'Panneau 2mx1m', 5, 1, 3, '', 78, 450, '2018-07-27', 450),
(398, 2, 1, 'Granneau Patrice', 'Directeur', 'Rue de l\'Arceau', 49300, 'Le Puy Saint Bonnet', 0, '', 'Panneau 2mx1m', 5, 1, 1, '', 49, 450, '2018-08-10', 450),
(399, 2, 1, 'Allaire Freddy', 'Directeur', 'Route d\'Yzernay ', 49360, 'LesCerqueux', 241295400, '', 'Jeux de maillots', 6, 1, 1, '', 28, 1800, '2018-10-11', 1800),
(400, 2, 1, 'Frouin Henri', 'Directeur', '10 rue de Terre Neuve', 49300, 'Cholet', 0, '', 'Encart programme de match', 6, 1, 1, '', 85, 110, '2018-08-31', 110),
(401, 2, 1, 'Carré Jean-Christophe', 'Directeur', '90 Avenue des Baux', 13520, 'Maussane les Alpilles', 0, '', 'Journée féminine', 6, 1, 1, 'Sans contre partie', 86, 500, '2018-08-18', 500),
(402, 2, 1, 'Charbonneau Alain', 'Directeur', '44 avenue de la Marne', 49300, 'Cholet', 0, '', 'Encart programme de match', 5, 1, 1, '', 87, 0, '0000-00-00', 70),
(403, 2, 1, 'Belin Jean-Louis', 'Directeur', '2 avenue Léon Gambetta', 49300, 'Cholet', 0, '', 'Encart journée féminine', 6, 1, 1, '', 88, 75, '2018-09-07', 75),
(404, 2, 1, 'Barré Pierrick', 'Gérant', '45 rue de Rambourg', 49300, 'Cholet', 0, '', 'Encart journée féminine', 6, 1, 1, '', 40, 50, '2018-09-18', 50),
(405, 2, 1, 'Beraud Frédéric ', '', '5 rue de la baie d Hudson', 49300, 'Cholet', 0, '', 'Billetterie journée feminine', 6, 1, 1, '', 178, 500, '2018-08-31', 500),
(406, 2, 1, 'Hugues Thomas', 'Directeur d\'agence', '36B avenue des Calins', 49300, 'Cholet', 0, '', 'Encart journée féminine', 6, 1, 1, '', 92, 75, '2018-08-21', 75),
(407, 2, 1, 'Joubert Thierry', 'Gérant', '5 rue des bons enfants', 49300, 'Cholet', 0, '', 'Encart dans tous les programmes', 6, 1, 1, '', 25, 200, '2018-10-22', 200),
(408, 2, 2, 'Cesbron Nathalie', 'Gérante', '61 rue de Lorraine', 49300, 'Cholet', 0, '', 'Encart programme de match', 6, 1, 1, '', 144, 60, '2018-09-08', 60),
(409, 2, 1, 'Rambaud Sébastien', 'Gérant', '27 rue Laënnec', 49300, 'Cholet', 0, '', 'Encart journée féminine', 6, 1, 1, '', 69, 75, '2018-09-22', 75),
(410, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 0, '', 'encart journée féminine', 6, 1, 1, '', 64, 75, '2018-09-06', 75),
(411, 2, 1, 'Charbonneau Alain', 'Directeur', '44 avenue de la Marne', 49300, 'Cholet', 0, '', 'Encart programme de match', 6, 1, 1, '', 87, 70, '2018-11-15', 70),
(412, 2, 1, '', '', '49 Boulevard de la Rontardière', 49300, 'Cholet', 241300300, '', 'Bons pour activités', 5, 3, 1, '', 82, 0, '0000-00-00', 50),
(414, 2, 1, 'Botton Frédéric', '', 'ZI de la Fosserie', 14600, 'HONFLEUR', 634134496, '', 'Jeu de maillots', 6, 1, 1, '', 207, 650, '2018-08-31', 650),
(415, 12, 1, 'GERMOND ROMAIN', 'GERANT', 'PLACE FRANCOIS MAURIAC', 49300, 'CHOLET', 611380057, 'romain.germond@mma.fr', 'panneau salle', 6, 1, 1, '', 208, 2000, '0000-00-00', 2000),
(416, 12, 2, 'GARCIA CELINE', '', '3 RUE DE LA MAILLOCHERE', 49300, 'CHOLET', 0, '', 'DON', 6, 1, 1, '', 209, 1000, '0000-00-00', 1000),
(417, 12, 1, 'RICHOU JEAN FRANCOIS', 'GERANT', '24 RUE SARDI CARNOT', 49300, 'CHOLET', 0, '', 'PRET MINIBUS', 6, 3, 1, '', 210, 0, '0000-00-00', 1665),
(418, 8, 1, 'Laurent CHENE', 'Co-Gérant', '', 49300, 'CHOLET', 0, 'laurentchene72@gmail.com', 'Panneau BL', 6, 1, 1, 'Oubli en 2015-2016', 179, 299, '2018-09-28', 299),
(419, 8, 1, 'JAUFRINEAU Philippe', 'Président', 'ZA du parc III, 35, rue du parc', 49280, 'ST CHRISTOPHE DU BOIS', 0, 'pj@tsh-emballages.com', 'Contrat Panneau', 6, 1, 1, 'Année 2 Panneau BL', 99, 300, '2018-10-03', 300),
(420, 1, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', 'Chéquier partenaires', 6, 2, 1, '', 160, 0, '0000-00-00', 500),
(421, 1, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', '', 6, 3, 1, 'Valorisation montant prestations en fin de contrat', 160, 0, '0000-00-00', 0),
(422, 1, 1, 'SONNIC', 'Gérant', '8 bis, route de Nantes', 85290, 'Mortagne sur Sèvre', 0, 'cyrille.sonnic@mma.fr', 'Chéquier partenaires JF', 6, 2, 3, 'Saison 1 contrat : 17/18', 117, 500, '0000-00-00', 500),
(423, 1, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 0, '', 'Brochure JF + événements JF + chéquier JF + minibus + autres', 6, 1, 2, 'Contrat global JF avec CMB, basket et tennis', 17, 5200, '0000-00-00', 5200),
(424, 1, 1, 'Gilbert NAUD', 'Directeur', '1 bld de la Victoire', 49300, 'CHOLET', 0, 'gilbert.naud@creditmutuel.fr', 'Encart brochure JF', 6, 2, 1, '', 65, 0, '0000-00-00', 500),
(425, 1, 1, 'Jean-Cahrales Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Chéquier partenaires + brochure + boutique + newsletter JF', 6, 2, 2, 'Contrat JF pour autres sections : CMB, basket, tennis', 46, 0, '0000-00-00', 2250),
(426, 13, 1, 'Jean-Charles Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Winter cup', 6, 3, 2, 'Contrat global JF - Valorisation 400 €', 46, 0, '0000-00-00', 0),
(427, 13, 1, 'Jean-Cahrales Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Winter cup', 5, 3, 3, 'Contrat global JF - Valorisation 400 €', 46, 0, '0000-00-00', 0),
(428, 3, 1, 'Jean-Cahrales Chabauty', 'Directeur', '8 rue du Lac Ontario', 49300, 'Cholet', 0, 'cholet@reseau-intersport.fr', 'Partenaire officiel', 6, 2, 2, 'Contrat globlal JF', 46, 0, '0000-00-00', 2750),
(429, 3, 1, 'Jacky GEINDREAU', 'Gérant', '45, avenue du Maréchal Leclerc', 49300, 'Cholet', 0, '', 'Encart programme', 5, 1, 1, '', 167, 250, '0000-00-00', 250),
(430, 3, 1, 'Jérôme FROUIN', 'Gérant', '45, avenue du Maréchal Leclerc', 49300, 'Cholet', 0, 'j.frouin@athexis.fr', 'Encart programme', 5, 1, 1, '', 211, 250, '0000-00-00', 250),
(431, 3, 1, 'SONNIC', 'Gérant', '2 rue du Brandon', 85500, 'LES HERBIERS', 2, 'cyrille.sonnic@mma.fr', 'Animation Défi du milieu de terrain', 5, 1, 3, 'Contrat global JF', 117, 1000, '0000-00-00', 1000),
(432, 3, 1, 'Gaëtan HUMEAU', 'Gérant', '5, rue du Grand Pré, ZI de l\'Ecuyère', 49300, 'CHOLET', 0, '', 'Encart programme', 5, 1, 1, '', 131, 500, '0000-00-00', 500),
(433, 3, 1, 'Bruno OGER', 'Gérant', '39 avenue Léon Gambetta', 49300, 'CHOLET', 0, '', '', 5, 1, 1, '', 212, 250, '0000-00-00', 250),
(434, 3, 1, 'Marc DELAYER', '', '24, rue de la Jominière CS21974', 49319, 'Cholet Cedex', 0, '', 'Encart programme + trophées 5 majeur', 5, 2, 1, '', 169, 1500, '0000-00-00', 1500),
(435, 3, 1, 'Legendre', 'Directeur', '10 Avenue de la Marne', 49300, 'Cholet', 2, 'inter.cholet@orange.fr', 'Partenaire officiel', 5, 2, 1, 'Dernière année du contrat de 3 ans, à renégocier pour 17-18', 45, 2000, '0000-00-00', 2000),
(436, 3, 1, 'Bruno AURIER', 'Directeur', '1, place des prairies', 49300, 'Cholet', 0, '', 'Encart programme + invitations partenaires', 5, 1, 1, 'Dernière année de sponsoring', 170, 800, '0000-00-00', 800),
(437, 3, 1, 'Christophe BILLAUD', 'Gérant', '3 avenue du Bois l\'Abbé', 49070, 'Beaucouzé', 0, '', 'Visibilité écrans', 5, 1, 1, 'Contact via clément MENARD', 213, 200, '0000-00-00', 200),
(438, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'CHAZELAT Aurore <achazelat@rgvgroupe.com>', 'Panneaux LED + naming tournoi', 5, 1, 1, '', 82, 1200, '0000-00-00', 1200),
(439, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'achazelat@rgvgroupe.com', 'Panneaux LED + annonces micro', 5, 1, 1, '', 174, 800, '0000-00-00', 800),
(440, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'achazelat@rgvgroupe.com', 'Concours de pronostics + annonces micros et écrans + distri flyers dans programme + distri flyers animations spéciales enfants avant CMB', 5, 2, 1, 'Relance règlement juin 2017', 175, 1480, '0000-00-00', 1480),
(441, 3, 2, 'Aurore Chazelat', 'Responsable com et marketing', '13 place de la République', 49300, 'Cholet', 632023678, 'CHAZELAT Aurore <achazelat@rgvgroupe.com>', 'Location locaux + prestation traiteur conférence de presse', 5, 3, 1, 'Valorisation montant prestatations : 1409,70 € HT', 82, 0, '0000-00-00', 0),
(442, 3, 1, 'Laurent GOHE', 'Direction Technique et Développements ', '6 rue de Langeais', 49300, 'Cholet', 0, 'laurent@fingersstyle.com', 'Encart programme', 5, 1, 1, '', 161, 500, '0000-00-00', 500),
(443, 3, 1, 'Jean-Yves PAPIN', 'Gérant', 'BP 23, 3 boulevard Jean Monnet', 49360, 'Maulévrier', 0, '', 'Partenaire officiel', 5, 1, 1, '', 164, 2750, '0000-00-00', 2750),
(444, 3, 1, 'Florent BARRE', 'Gérant', '30 rue du Carteron', 49300, 'CHOLET', 0, '', '', 5, 1, 1, '', 214, 700, '0000-00-00', 700),
(445, 3, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', 'Ballon du match', 5, 1, 1, '', 160, 500, '0000-00-00', 500),
(446, 3, 2, 'FRÉDÉRIQUE TUSSEAU', 'Assistante commerciale', '7 rue Savary', 49100, 'ANGERS', 2, 'f.tusseau@cafpi.fr', 'Tenue des bénévoles', 5, 2, 3, 'Contrat global JF', 122, 2000, '0000-00-00', 2000),
(447, 3, 2, 'Aurélie Giraud', 'Responsable des ressources humaines', ' 37 rue Pierre et Marie Curie - BP 10966', 49309, 'CHOLET Cedex', 0, '', 'Don sans contrepartie (encart programme offert)', 5, 1, 1, '', 193, 900, '0000-00-00', 900),
(449, 1, 2, 'Céline Lumineau', 'Chargée de communication', '2 rue de la Baie d\'Hudson', 49300, 'Cholet', 241707414, 'celine.biocoop-soleil.sud@orange.fr', 'Encart brochure JF + logo sur site internet + partenaire de 2 événements', 6, 1, 1, '', 54, 0, '0000-00-00', 2100),
(450, 13, 1, 'Gilbert NAUD', 'Directeur', '1 bld de la Victoire', 49300, 'CHOLET', 0, 'gilbert.naud@creditmutuel.fr', 'Encart brochure JF', 5, 2, 1, '', 65, 600, '0000-00-00', 600),
(451, 13, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 2, '', 'Brochure JF + événements JF + chéquier JF + minibus + autres', 5, 1, 2, 'Contrat global JF', 17, 1000, '0000-00-00', 1000),
(452, 3, 1, 'SONNIC', 'Gérant', '2 rue du Brandon', 85500, 'LES HERBIERS', 2, 'cyrille.sonnic@mma.fr', 'Animation Défi du milieu de terrain', 6, 1, 2, 'Contrat global JF', 117, 1000, '0000-00-00', 1000),
(453, 1, 1, 'Landais Bruno', 'Associé', '11 rue du Charolais', 49300, 'Cholet', 0, '', 'Encart brochure JF', 6, 2, 1, '', 130, 500, '0000-00-00', 500),
(454, 3, 1, 'Thomas Bourgault', 'Gérant', '172 rue de Lorraine ', 49300, 'Cholet', 0, 'bourgault.thomas@gmail.com', 'Animation Ballon du match', 6, 2, 1, '', 160, 0, '0000-00-00', 500),
(455, 1, 1, 'Philippe Legendre', 'Directeur', '', 49300, 'Cholet', 241752467, 'inter.cholet@orange.fr', 'Encart carte adhérents + encart Brochure JF', 6, 2, 1, '', 45, 0, '0000-00-00', 800),
(456, 3, 2, 'FRÉDÉRIQUE TUSSEAU', 'Assistante commerciale', '7 rue Savary', 49100, 'ANGERS', 0, 'f.tusseau@cafpi.fr', 'Tenue des bénévoles', 6, 2, 2, 'Contrat global JF', 122, 0, '0000-00-00', 2000),
(457, 1, 2, 'FRÉDÉRIQUE TUSSEAU', 'Assistante commerciale', '7 rue Savary', 49100, 'ANGERS', 0, 'f.tusseau@cafpi.fr', 'Tenue des bénévoles', 6, 2, 3, 'Contrat global JF', 122, 0, '0000-00-00', 500),
(458, 3, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 2, '', 'Encart programme', 6, 1, 1, 'Contrat global JF', 17, 0, '0000-00-00', 500),
(459, 13, 1, 'Mickael BRUNET', 'Directeur de magasin', 'boulevard Jacques Cassini Zac Des, Boulevard du Cormier', 49300, 'CHOLET', 0, '', 'Winter cup', 5, 2, 1, 'Changement de directeur pour la saison prochaine', 3, 350, '0000-00-00', 350),
(460, 13, 1, 'Proux', 'PDG', '31 rue David d\'Anger', 49122, 'Le May sur Evre', 0, '', 'Winter cup', 5, 2, 1, '', 47, 300, '0000-00-00', 300),
(461, 13, 1, '', '', '', 0, '', 0, '', 'Winter cup', 5, 1, 1, '', 215, 607, '0000-00-00', 607),
(462, 13, 1, '', '', '', 0, '', 0, '', '', 5, 1, 1, '', 216, 300, '0000-00-00', 300),
(463, 13, 1, '', '', '', 0, '', 0, '', 'Winter cup', 5, 1, 1, '', 217, 300, '0000-00-00', 300),
(464, 13, 1, '', '', '', 0, '', 0, '', 'Winter cup', 5, 1, 1, '', 218, 300, '0000-00-00', 300),
(465, 13, 1, '', '', '', 0, '', 0, '', 'Winter cup', 5, 1, 1, '', 219, 150, '0000-00-00', 150),
(466, 13, 1, '', '', '', 0, '', 0, '', 'Winter cup', 5, 1, 1, '', 220, 200, '0000-00-00', 200),
(467, 1, 1, 'Laranger', 'Directeur', '45 rue Alphonse Darmaillacq', 49300, 'CHOLET', 241494200, '', 'Encart brochure JF', 6, 2, 1, '', 221, 0, '0000-00-00', 350),
(468, 2, 1, '', 'Associé', '37 Avenue de la Tessoualle', 49300, 'Cholet', 241565046, '', 'Panneau 2mx1m', 6, 1, 1, '', 53, 450, '2018-10-05', 450),
(469, 2, 1, 'Jallier et Tharreau', 'Gérant', 'Zi de la Blanchardière', 49300, 'Cholet', 241622296, '', 'Marquage au sol', 6, 1, 2, '', 30, 1000, '2018-10-18', 1000),
(470, 9, 2, 'POIROUT Noëlle', 'Gérante', '5 rue Honoré Neveu', 49122, 'Le May sur Evre', 614308418, 'noelle.poirout@gmail.com', 'Présence du partenaire sur le programme du Gala et des compétitions 2018-2019', 6, 1, 1, '', 222, 400, '2018-10-20', 400),
(471, 2, 1, 'Jobard Alain', 'Gérant', '2 rue Jean Monnet', 85130, 'La Verrie', 241659595, '', 'Jeu de maillots', 6, 1, 1, '', 201, 500, '2018-10-24', 500),
(472, 9, 1, 'Jean Yves PAPIN', 'PDG', 'Route de Cholet', 49360, 'MAULEVRIER', 0, 'info@pact-europact.com', 'Présence du partenaire sur le programme du Gala et des compétitions 2018-2019', 6, 1, 1, '', 164, 300, '2018-10-26', 300),
(473, 9, 1, 'David BAUCHET', 'Gérant', '15 Rue Cathelineau', 49300, 'CHOLET', 241620756, '', 'Don en nature baguettes, viennoiseries, mignardises pour le GALA et compétitions', 7, 3, 1, '', 223, 100, '2018-11-17', 100),
(474, 2, 1, 'Saudeau', 'Gérant', 'Les Audouins', 49280, 'St Léger sous Cholet', 241562430, 'saudeau.sarl@wanadoo.fr', 'Panneau 2mx1m', 6, 1, 2, '', 55, 500, '2018-11-13', 500),
(475, 2, 1, 'Rousseau Jocelyn', 'Directeur', '184 rue de Lorraine', 49300, 'Cholet', 660049891, '', 'Panneau 2mx1m', 5, 1, 1, '', 64, 400, '2018-11-19', 400),
(476, 2, 1, 'Rousseau', 'Gérant', '', 49300, 'Cholet', 659418349, '', 'Panneau 2m x 1m', 6, 1, 3, '', 202, 250, '2018-09-15', 500),
(477, 2, 1, 'Naud Gilbert', 'Directeur d\'agence', '1 Bld de la Victoire', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 6, 1, 1, 'Contrat JF global', 65, 450, '2018-12-15', 450),
(478, 2, 1, 'VAIGNEAU', 'Président', '1 rue de la Sarthe', 49300, 'Cholet', 0, '', 'Panneau 2mx1m', 6, 1, 1, 'Contrat JF global', 17, 500, '0000-00-00', 500);

-- --------------------------------------------------------

--
-- Table structure for table `prestation`
--

CREATE TABLE `prestation` (
  `idPrestation` int(4) NOT NULL,
  `nomPrestation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prestation`
--

INSERT INTO `prestation` (`idPrestation`, `nomPrestation`) VALUES
(1, 'mécénat'),
(2, 'sponsoring'),
(3, 'Mécénat nature et compétences');

-- --------------------------------------------------------

--
-- Table structure for table `saison`
--

CREATE TABLE `saison` (
  `idSaison` int(4) NOT NULL,
  `nomSaison` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saison`
--

INSERT INTO `saison` (`idSaison`, `nomSaison`) VALUES
(3, '2015/2016'),
(4, '2016/2017'),
(5, '2017/2018'),
(6, '2018/2019'),
(7, '2019/2020'),
(8, '2020/2021');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `idSection` int(4) NOT NULL,
  `nomSection` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`idSection`, `nomSection`) VALUES
(1, 'JF'),
(2, 'BasketBall'),
(3, 'Cholet Mondial BasketBall'),
(6, 'Billard'),
(8, 'Football'),
(9, 'Gymnastique'),
(10, 'Judo Taiso'),
(11, 'Shorinji Kempo'),
(12, 'Roller Hockey'),
(13, 'Tennis'),
(14, 'Tennis tournoi Europe'),
(15, 'Danse'),
(16, 'Roller Détente');

-- --------------------------------------------------------

--
-- Table structure for table `typepartenaire`
--

CREATE TABLE `typepartenaire` (
  `idTypePartenaire` int(11) NOT NULL,
  `nomTypePartenaire` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `typepartenaire`
--

INSERT INTO `typepartenaire` (`idTypePartenaire`, `nomTypePartenaire`) VALUES
(3, 'Leroy Merlin'),
(4, 'Aviva'),
(6, 'Opticiens Atol'),
(7, 'Oger Lefreche'),
(8, 'Bricolow'),
(136, 'SARL Rigaudeau/Jeanneau/Christiaens'),
(10, 'SPPEC'),
(11, 'Ets Saudeau'),
(12, 'Crédit Agricole'),
(13, 'Graphic 49'),
(17, 'Mutuelle Choletaise'),
(15, 'AXA'),
(18, 'Indepimmo'),
(19, 'Château de la Tremblay'),
(20, 'Le Fleuron des Mauges'),
(21, 'Garage AD'),
(22, 'Paprec plastiques'),
(24, 'Idem'),
(25, 'L\'annexe'),
(26, 'Vert et bleu'),
(27, 'Artisport'),
(28, 'Pasquier'),
(29, 'Packalim'),
(30, 'Négoce auto'),
(31, 'Opticiens mutualistes'),
(32, 'ADF services'),
(33, 'Mc Do Cholet'),
(34, 'Transports RAUD'),
(35, 'SRI'),
(36, 'SCV'),
(37, 'La cave du Reve'),
(38, 'Relais de la borderie'),
(39, 'Sarl Bequet'),
(40, 'Fromage et compagnie'),
(41, 'A louer vaisselle'),
(42, 'mélusine'),
(43, 'ECF'),
(44, 'PK3 SACOMI'),
(45, 'Intermarché'),
(46, 'Intersport'),
(47, 'Palamy'),
(48, 'Coiffure Chambiron'),
(49, 'AES'),
(50, 'EURL Guyomarch'),
(52, 'La boîte a pizza'),
(53, 'ABG climatique'),
(54, 'Biocoop'),
(55, 'Saudeau'),
(56, 'PretPartners'),
(57, 'Côté cour'),
(58, 'Quadra'),
(59, 'Annett'),
(60, 'Cholet TP'),
(61, 'GSF Aurica'),
(62, 'GRS'),
(63, 'Travers'),
(64, 'Effets de couleur'),
(65, 'Credit Mutuel'),
(66, 'Décor Moreau'),
(67, 'Gravlux'),
(68, 'Sibauto'),
(69, 'Boucherie du Parc'),
(70, 'MEDI 49'),
(71, 'CS3I'),
(135, 'FROID CEMI'),
(73, 'Piscines Cholet'),
(74, 'Hertz'),
(133, 'GEODIS Calberson'),
(132, 'TOYOTA GCA Cholet'),
(78, 'Renoval'),
(79, 'Audition conseil'),
(80, 'AVIVA'),
(81, 'Faubourg café'),
(82, 'L\'autre usine'),
(83, 'Cholet poids lourds'),
(84, 'Le Bord du Lac'),
(85, 'AB Services'),
(86, 'Sarl  à votre service'),
(87, 'Clean dicount'),
(88, 'Compagnons cavistes'),
(89, 'Dugast Chrystelle'),
(90, 'Ecrivain public'),
(91, 'Garage du Bocage'),
(92, 'Thelem assurance'),
(93, 'Garage du Parc'),
(94, 'Patapain'),
(95, 'ABG Groupe C\'pro'),
(96, 'Naturorama'),
(97, 'Pure'),
(98, 'Boucherie Leroux'),
(99, 'TSH Emballages'),
(100, 'PINEAU & Fils'),
(101, 'SIMA'),
(102, 'BOUCHET & Fils'),
(103, 'Philippe JANNIERE'),
(131, 'GYT'),
(105, 'NOYER'),
(106, 'CEFW'),
(107, 'Mgr Notot'),
(108, 'Bequet'),
(109, 'Bouchet & Fils'),
(110, 'CCIP LARRIBEAU'),
(111, 'SARL LANDREAU'),
(112, 'Mensuiserie JANNIERE'),
(113, 'Pineau & Fils'),
(114, 'SA Jean Yves NOYER'),
(134, 'ALGIMOUSS'),
(116, 'La ronde des pains'),
(117, 'MMA'),
(118, 'toto'),
(119, 'HelioCar-Jean Rouyer'),
(120, 'SARL CHENE'),
(121, 'BORDRON ELECTRICITE'),
(122, 'CAPFI'),
(123, 'Le Partenaire du Peintre'),
(124, 'ISO FACADES'),
(125, 'Bois Bocage Environnement'),
(126, 'DMT CONCEPT'),
(127, 'LOISEAU Alain'),
(128, 'Le Pro Etanch'),
(129, 'Matthieu GIBT'),
(130, 'ENERGIES CHOLETAISES'),
(137, 'SCMC'),
(138, 'SC Deveaud'),
(139, 'CEPY'),
(140, 'Romain Brebion Couverture'),
(141, 'Collection Cuisine'),
(142, 'Patrick Renou financements'),
(144, 'La Lorraine'),
(145, 'Espace du Maine'),
(146, 'Boucherie Lemesle'),
(147, 'T.C.S.'),
(148, 'Tabac Le Périph'),
(149, 'Coiffeur Aguille'),
(150, 'Axa Hervé Meneust'),
(151, 'Axa Merlet-Tison'),
(152, 'Robineau Orthopédie'),
(153, 'Tabac Le Renitas'),
(154, 'Taxi Duchesne'),
(155, 'Jallier Tharreau'),
(156, 'Ferme des Mille Pieds'),
(157, 'Eric Coulonnier'),
(158, 'Boulangerie Cirou'),
(159, 'Batimpro'),
(160, 'Avenue des fleurs'),
(161, 'SARL Fingers style'),
(162, 'SNC Pharmacie des 2 Lacs'),
(163, 'NEWCLIP TECHNICS SAS'),
(164, 'PACT EUROPACT'),
(165, 'BATEL 49 Cholet'),
(166, 'BASTAT FROUIN EXPERTISES'),
(167, 'ATHEXIS Cholet'),
(168, 'AVANTI TELECOM'),
(169, 'Choletbus TPC'),
(170, 'CHARAL'),
(171, 'Saveurs des Mauges'),
(172, 'BODET'),
(173, 'ESP Sécurité'),
(174, 'L\'Autre Faubourg'),
(175, 'RGV Groupe'),
(176, 'Mode Eco Logis'),
(177, 'Effets de matière'),
(178, 'Toyota'),
(179, 'Chéné SARL'),
(180, 'LS Auto Premium'),
(181, 'FC Balayage'),
(182, 'Soulard Electricité'),
(183, 'Boutique Gourmande'),
(184, 'AUGEREAU Gaec'),
(185, 'Akaze production'),
(186, 'RENOU EARL'),
(187, 'FONTENEAU Immobilier'),
(188, 'Aux Dix Ecus'),
(189, 'Pélissier Maçonnerie'),
(190, 'Oger Frères'),
(191, 'Clémot Immobilier'),
(192, 'SIEMENS'),
(193, 'Nicoll'),
(194, 'Au Guingois'),
(195, 'Vergers de la Chenillère'),
(196, 'OPTIFINANCE'),
(197, 'ASSUR\'INVEST'),
(198, 'ABJ - PUBLICITE'),
(199, 'AKATHERM'),
(200, 'CARREFOUR'),
(201, 'SARL Jobard'),
(202, 'Les 2 peintres'),
(203, 'Les 2 peintres'),
(204, 'Fingers Style'),
(205, 'Carrosserie Castin'),
(206, 'blabla'),
(207, 'TRIMA'),
(208, 'SARL GERMOND ASSOCIES'),
(209, 'GARCIA CELINE'),
(210, 'RICHOU VOYAGES'),
(211, 'SARL BFE'),
(212, 'JBL Conseil'),
(213, 'TELELOGOS'),
(214, 'SARL TOTEM'),
(215, 'WOUTER'),
(216, 'SOVILEG'),
(217, 'KIA'),
(218, 'CHARRIER BMW'),
(219, 'IBIS BUDGET'),
(220, 'CLEON'),
(221, 'ST JOSEPH'),
(222, 'TUTTI PIZZA LE MAY/EVRE'),
(223, 'LES DELICES DE CHARLOTTE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `civilitecontact`
--
ALTER TABLE `civilitecontact`
  ADD PRIMARY KEY (`idCiviliteContact`);

--
-- Indexes for table `codeacces`
--
ALTER TABLE `codeacces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `duree`
--
ALTER TABLE `duree`
  ADD PRIMARY KEY (`idDuree`);

--
-- Indexes for table `partenaire`
--
ALTER TABLE `partenaire`
  ADD PRIMARY KEY (`idPartenaire`);

--
-- Indexes for table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`idPrestation`);

--
-- Indexes for table `saison`
--
ALTER TABLE `saison`
  ADD PRIMARY KEY (`idSaison`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`idSection`);

--
-- Indexes for table `typepartenaire`
--
ALTER TABLE `typepartenaire`
  ADD PRIMARY KEY (`idTypePartenaire`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `civilitecontact`
--
ALTER TABLE `civilitecontact`
  MODIFY `idCiviliteContact` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `codeacces`
--
ALTER TABLE `codeacces`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `duree`
--
ALTER TABLE `duree`
  MODIFY `idDuree` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partenaire`
--
ALTER TABLE `partenaire`
  MODIFY `idPartenaire` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=479;

--
-- AUTO_INCREMENT for table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `idPrestation` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `saison`
--
ALTER TABLE `saison`
  MODIFY `idSaison` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `idSection` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `typepartenaire`
--
ALTER TABLE `typepartenaire`
  MODIFY `idTypePartenaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
