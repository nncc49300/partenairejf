<?php

if(!isset($_REQUEST['action']))
     $action = 'affichageTableau';
else
	$action = $_REQUEST['action'];

switch($action)
{
	case 'affichageTableau':
	{
		$lesSections=$pdo->getLesSections();
		$lesTypePartenaires=$pdo->getLesTypePartenaires();
		$lesSaisons=$pdo->getLesSaisons();

		if(isset($_GET['filtre'])){
			if($_GET['filtre']=="section"){
				$_SESSION['filtreSection']=$_GET['value'];
			}
			if($_GET['filtre']=="typePartenaire"){
				$_SESSION['filtreTypePartenaire']=$_GET['value'];
			}
			if($_GET['filtre']=="saison"){
				$_SESSION['filtreSaison']=$_GET['value'];
			}
		}

		$lesPartenaires = $pdo->getLesPartenairesFiltre($_SESSION['filtreSection'],$_SESSION['filtreTypePartenaire'],$_SESSION['filtreSaison']);
		
		include("view/v_tableau.php");
		break;
	}

	case 'modifierPartenaire':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$unPartenaire=$pdo->getUnPartenaire($idPartenaire);
		$lesSections=$pdo->getLesSections();
		$lesCivilites=$pdo->getLesCivilites();
		$lesPrestations=$pdo->getLesPrestations();
		$lesSaisons=$pdo->getLesSaisons();
		$lesDurees=$pdo->getLesDurees();
		$lesTypePartenaires = $pdo->getLesTypePartenaires();
		include("view/v_modifPartenaire.php");
		break;
	}

	case 'modifierPartenaireScript':
	{
		$idPartenaire=$_REQUEST['idPartenaire'];
		$section_id=$_REQUEST['section_id'];
		$typePartenaire_id=$_REQUEST['typePartenaire_id'];
		$civiliteContact_id=$_REQUEST['civiliteContact_id'];
		$nomContact=$_REQUEST['nomContact'];
		$fonctionContact=$_REQUEST['fonctionContact'];
		$adresse=$_REQUEST['adresse'];
		$CP=$_REQUEST['CP'];
		$ville=$_REQUEST['ville'];
		$tel=$_REQUEST['tel'];
		$mail=$_REQUEST['mail'];
		$prestation=$_REQUEST['prestation'];
		$prestation_id=$_REQUEST['prestation_id'];
		$saison=$_REQUEST['saison_id'];
		$duree_id=$_REQUEST['duree_id'];
		$remarques=$_REQUEST['remarques'];
		$montant=$_REQUEST['montant'];
		$datePaiement=$_REQUEST['datePaiement'];
		$montantPromis=$_REQUEST['montantPromis'];

		$pdo->modifierPartenaire($idPartenaire, $section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis);

		echo "Contrat bien modifié. Vous allez être redirigé vers la page d'accueil.";
		
		?>
		<script type="text/javascript">
			//document.location.href = "index.php?uc=index.php?uc=listePartenaire"
			 setTimeout(function () {
		       window.location.href = "index.php?uc=listePartenaire"; //will redirect to your blog page (an ex: blog.html)
		    }, 2000); //will call the function after 2 secs.
		</script>
		<?php

		break;
	}

	case 'ajoutPartenaire':
	{
		$lesSections=$pdo->getLesSections();
		$lesCivilites=$pdo->getLesCivilites();
		$lesPrestations=$pdo->getLesPrestations();
		$lesSaisons=$pdo->getLesSaisons();
		$lesDurees=$pdo->getLesDurees();
		$lesTypePartenaires = $pdo->getLesTypePartenaires();
		include("view/v_ajoutPartenaire.php");
		break;
	}

	case 'ajoutPartenaireScript':
	{
		$section_id=$_REQUEST['section_id'];
		$typePartenaire_id=$_REQUEST['typePartenaire_id'];
		$civiliteContact_id=$_REQUEST['civiliteContact_id'];
		$nomContact=$_REQUEST['nomContact'];
		$fonctionContact=$_REQUEST['fonctionContact'];
		$adresse=$_REQUEST['adresse'];
		$CP=$_REQUEST['CP'];
		$ville=$_REQUEST['ville'];
		$tel=$_REQUEST['tel'];
		$mail=$_REQUEST['mail'];
		$prestation=$_REQUEST['prestation'];
		$prestation_id=$_REQUEST['prestation_id'];
		$saison=$_REQUEST['saison_id'];
		$duree_id=$_REQUEST['duree_id'];
		$remarques=$_REQUEST['remarques'];
		$montant=$_REQUEST['montant'];
		$datePaiement=$_REQUEST['datePaiement'];
		$montantPromis=$_REQUEST['montantPromis'];

		$pdo->ajoutPartenaire($section_id, $typePartenaire_id, $civiliteContact_id, $nomContact, $fonctionContact, $adresse, $CP, $ville, $tel, $mail, $prestation, $prestation_id, $saison, $duree_id, $remarques, $montant, $datePaiement, $montantPromis);

		echo "Contrat ajouté. Vous allez être redirigé vers la page d'accueil.";
		
		/*
		?>
		<script type="text/javascript">
			//document.location.href = "index.php?uc=index.php?uc=listePartenaire"
			 setTimeout(function () {
		       window.location.href = "index.php?uc=listePartenaire"; //will redirect to your blog page (an ex: blog.html)
		    }, 2000); //will call the function after 2 secs.
		</script>
		<?php
		*/
		
		break;
	}

	case 'ajoutTypePartenaire':
	{
		$lesTypePartenaires=$pdo->getLesTypePartenaires();
		include("view/v_ajoutTypePartenaire.php");
		break;
	}

	case 'ajoutTypePartenaireScript':
	{
		$nomTypePartenaire=$_REQUEST['nomTypePartenaire'];
		$pdo->ajoutTypePartenaire($nomTypePartenaire);
		echo "Partenaire bien ajouté. Vous allez être redirigé vers la page d'accueil.";
		header( "refresh:3;url=index.php?uc=gestionPartenaire&action=ajoutPartenaire" );
		break;
	}

    case 'supprimerPartenaire':
    {
        $idPartenaire = $_REQUEST['idPartenaire'];
        $pdo->supprimerPartenaire($idPartenaire);
        ?>
        <script type="text/javascript">
            window.location.href = "index.php?uc=listePartenaire";
        </script>
        <?php
        break;
    }

	case 'exportCSV':
	{
		/*
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		
		// output the column headings
		fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

		// fetch the data
		mysql_connect('localhost', 'username', 'password');
		mysql_select_db('database');
		$rows = mysql_query('SELECT field1,field2,field3 FROM table'); 

		$lesPartenaires = $pdo->getLesPartenairesFiltre($_SESSION['filtreSection'],$_SESSION['filtreTypePartenaire'],$_SESSION['filtreSaison']);

		function cleanData(&$str)
		  {
		    $str = preg_replace("/\t/", "\\t", $str);
		    $str = preg_replace("/\r?\n/", "\\n", $str);
		    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
		  }

		  // filename for download
		  $filename = "website_data_" . date('Ymd') . ".xls";

		  header("Content-Disposition: attachment; filename=\"$filename\"");
		  header("Content-Type: application/vnd.ms-excel");

		  $flag = false;
		  foreach($lesPartenaires as $unPartenaire) {
		    if(!$flag) {
		      // display field/column names as first $unPartenaire
		      echo implode("\t", array_keys($unPartenaire)) . "\r\n";
		      $flag = true;
		    }
		    array_walk($unPartenaire, __NAMESPACE__ . '\cleanData');
		    echo implode("\t", array_values($unPartenaire)) . "\r\n";
		  } */


		  
		break;
	}
	

}


?>


