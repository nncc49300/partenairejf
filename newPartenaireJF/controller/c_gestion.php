<?php

if(!isset($_REQUEST['action']))
    $action = 'gestion_document';
else
	$action = $_REQUEST['action'];

switch($action)
{
	case 'gestion_documents':
		{
			$lesDocuments=$pdo->getLesDocuments();
			include("view/v_gestion_documents.php");
			break;
		}

	case 'gestion_partenaires':
	{
	    $lesTypePartenaires=$pdo->getLesTypePartenaires();
		include("view/v_gestion_partenaires.php");
		break;
	}

	case 'modifier_document':
	{
		$uploaddir = 'res/documents/';
		$fichierDocument = basename($_FILES['fichierDocument']['name']);
		$uploadfile = $uploaddir.$fichierDocument;

		if (move_uploaded_file($_FILES['fichierDocument']['tmp_name'], $uploadfile)) {
			$idDocument=$_REQUEST['idDocument'];
			$anneeDocument=$_REQUEST['anneeDocument'];
			$pdo->modifierDocument($idDocument, $anneeDocument, $fichierDocument);
			?>
			<script type="text/javascript">
				//document.location.href = ""
				window.alert('Fichier correctement importé');
				window.location.href='index.php?uc=gestion&action=gestion_documents';
			</script>
			<?php
		} else {
			?>
			<script type="text/javascript">
				//document.location.href = ""
				window.alert("Erreur lors de l'import, veuillez contacter nicolas.chauvigne@yahoo.fr");
				window.location.href='index.php?uc=gestion&action=gestion_documents';
			</script>
			<?php
		}

		break;
	}

    case 'ajoutTypePartenaire':
    {
        $nomTypePartenaire=$_REQUEST['nomTypePartenaire'];
        $pdo->ajoutTypePartenaire($nomTypePartenaire);
        ?>
        <script type="text/javascript">
            window.alert('Partenaire ajouté');
            window.location.href='index.php?uc=gestion&action=gestion_partenaires';
        </script>
        <?php
        break;
    }

    case 'modifierTypePartenaire':
    {
        $idTypePartenaire=$_REQUEST['idTypePartenaire'];
        $nomTypePartenaire=$_REQUEST['nomTypePartenaire'];
        $pdo->modifierTypePartenaire($nomTypePartenaire, $idTypePartenaire);
        ?>
        <script type="text/javascript">
            window.alert('Partenaire modifié');
            window.location.href='index.php?uc=gestion&action=gestion_partenaires';
        </script>
        <?php
        break;
    }

    case 'supprimerTypePartenaire':
    {
        $idTypePartenaire=$_REQUEST['idTypePartenaire'];
        $pdo->supprimerTypePartenaire($idTypePartenaire);
        ?>
        <script type="text/javascript">
            window.alert('Partenaire supprimé');
            window.location.href='index.php?uc=gestion&action=gestion_partenaires';
        </script>
        <?php
        break;
    }
}


?>


