<?php
session_start();
require_once("model/class.pdoPartenaireJf.inc.php");
require_once("model/functions.php");
include("view/v_header.php");
$pdo = PdoPartenaireJf::getPdoPartenaireJf();

if(isset($_SESSION['loginLevel']))
{
  if(!isset($_REQUEST['uc'])){
    $uc = 'listePartenaire';
    $_SESSION['filtreSaison'] = 7;
    $_SESSION['filtreSection'] = 0;
    $_SESSION['filtreTypePartenaire'] = 0;
  }
  else
    $uc = $_REQUEST['uc'];
}
else
  $uc = 'login';


switch($uc){

  case 'documents':
  {
    include("view/v_menu.php");
    include("controller/c_documents.php");
    break;
  }

  case 'listePartenaire':
  {
    include("view/v_menu.php");
    include("controller/c_partenaire.php");
    break;
  }

  case 'statistiques':
  {
    include("view/v_menu.php");
    include("controller/c_statistiques.php");
    break;
  }

    case 'gestion':
    {
        if(!verifLevelAdmin(1)) {
            ?>
            <script type="text/javascript">
                window.location.replace('404.html');
            </script>
            <?php
        }
        include("view/v_menu.php");
        include('controller/c_gestion.php');
        break;
    }

  case 'login':
  {
    include("controller/c_login.php");
    break;
  }

  case 'logout':
  {
    session_destroy();
    ?>
    <script type="text/javascript">
      window.location.replace('index.php');
    </script>
    <?php
    break;
  }

  default :
  {
    ?>
    <script type="text/javascript">
      window.location.replace('404.html');
    </script>
    <?php
    break;
  }
}

include("view/v_footer.php");

?>

