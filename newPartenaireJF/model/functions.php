<?php 
function getActualSeason(){
	$actualMonth = date('m', time());
	$actualYear = date('Y', time());
	$nextYear = (int)$actualYear+1;
	$previousYear = (int)$actualYear-1;

	if($actualMonth > "06"){
		$actualSeason = $actualYear + "/" + $nextYear;
	}else{
		$actualSeason = $previousYear + "/" + $actualYear;
	}
	return $actualSeason;
}

function percent($val, $max){
	$newVal = ($val / $max)*100;
	return $newVal;
}

function verifLevelAdmin($loginLevel){
    if($_SESSION['loginLevel'] == $loginLevel){
        return true;
    }else{
        return false;
    }
}
?>