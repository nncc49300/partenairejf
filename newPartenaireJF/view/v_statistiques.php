<h3><i class="fa fa-angle-right"></i>Statistiques</h3>
<div class="nbPartenariat">
	<div class="border-head">
	  <h3>Nombre de partenariat par section</h3>
	</div>
	<div class="custom-bar-chart">
	  <ul class="y-axis">
	    <li><span>500</span></li>
	    <li><span>400</span></li>
	    <li><span>300</span></li>
	    <li><span>200</span></li>
	    <li><span>100</span></li>
	    <li><span>0</span></li>
	  </ul>
	  	<?php 
		foreach ($lesPartenariats as $unPartenariat)
		{
			$nbPartenariatPercent = percent($unPartenariat['nbPartenariat'], $MAX);
			?>
			<div class="bar">
			    <div class="title"><?php echo $unPartenariat['nomSection']; ?></div>
			    <div class="value tooltips" data-original-title="<?php echo $unPartenariat['nbPartenariat']; ?>" data-toggle="tooltip" data-placement="top"><?php echo $nbPartenariatPercent;?>%</div>
			</div>
			<?php
		}
		?>
	</div>
</div>
