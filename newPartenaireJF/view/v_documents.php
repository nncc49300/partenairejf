<div class="row">
	<div class="col-sm-12">
		<h3><i class="fa fa-angle-right"></i>Documents</h3>
	</div>
</div>

<div class="row mt">
  <div class="col-md-12">
  	<div class="content-panel">
	    <section class="task-panel tasks-widget">
	      <div class="panel-heading">
	        <div class="pull-left">
	          <h4><i class="fa fa-tasks"></i> Liste des documents</h4>
	        </div>
	        <br>
	      </div>
	      <div class="panel-body">
	        <div class="task-content">
	          <ul class="task-list">
                  <?php
                  foreach ($lesDocuments as $unDocument)
                  {
                      ?>
                      <li>
                          <div class="task-title">
                              <div class="todo-title">
                                  <span class="task-title-sp"><?php echo $unDocument['nomDocument'];?></span>
                                  <span class="badge bg-success"><?php echo $unDocument['anneeDocument'];?></span>
                                  <div class="pull-right hidden-phone">
                                      <a href="res/documents/<?php echo $unDocument['fichierDocument'];?>" target="_blank"><button class="btn btn-info btn-xs">Télécharger</button></a>
                                  </div>
                              </div>
                          </div>
                      </li>
                      <?php
                  }
                  ?>
	          </ul>
	        </div>
	      </div>
	    </section>
	</div>
  </div>
  <!-- /col-md-12-->
</div>
<!-- /wrapper -->
