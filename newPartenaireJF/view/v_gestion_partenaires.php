<div class="row">
    <div class="col-sm-6">
        <h3><i class="fa fa-angle-right"></i>Gestion des partenaires</h3>
    </div>
    <div class="col-sm-6" style="text-align: right; margin: 1% 0 0 0;">
        <a href="#ajoutPartenaire" data-toggle="modal"><button class="btn btn-round btn-success"">Nouveau partenaire</button></a>
    </div>
</div>

<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>Nom du partenaire</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($lesTypePartenaires as $unTypePartenaire)
                { ?>
                    <tr>
                        <td><?php echo $unTypePartenaire['nomTypePartenaire']; ?></td>
                        <td>
                            <a href="#modifierPartenaire<?php echo $unTypePartenaire['idTypePartenaire']; ?>" data-toggle="modal"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                            <a href="#supprimerPartenaire<?php echo $unTypePartenaire['idTypePartenaire']; ?>" data-toggle="modal"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                        </td>
                    </tr>
                    <!-- Modal supprimer partenaire (admin only)-->
                    <div class="modal fade modal-supprimerPartenaire" id="supprimerPartenaire<?php echo $unTypePartenaire['idTypePartenaire']; ?>" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Confirmation</h4>
                                </div>
                                <div class="modal-body">
                                    Voulez-vous vraiment supprimer ce partenaire ?
                                </div>
                                <div class="modal-footer">
                                    <a href="index.php?uc=gestion&action=supprimerTypePartenaire&idTypePartenaire=<?php echo $unTypePartenaire['idTypePartenaire']; ?>">
                                        <button type="button" class="btn btn-danger">Supprimer</button>
                                    </a>
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal modifier partenaire (admin only)-->
                    <div class="modal fade modal-supprimerPartenaire" id="modifierPartenaire<?php echo $unTypePartenaire['idTypePartenaire']; ?>" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <form method="post" action="index.php?uc=gestion&action=modifierTypePartenaire&idTypePartenaire=<?php echo $unTypePartenaire['idTypePartenaire']; ?>">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Modifier le partenaire</h4>
                                    </div>
                                    <div class="modal-body">
                                        Nom du partenaire : <input type="text" name="nomTypePartenaire" size="35" value="<?php echo $unTypePartenaire['nomTypePartenaire']; ?>">
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Valider">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Modal ajouter partenaire (admin only)-->
                    <div class="modal fade modal-supprimerPartenaire" id="ajoutPartenaire" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <form method="post" action="index.php?uc=gestion&action=ajoutTypePartenaire">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Ajouter un partenaire</h4>
                                    </div>
                                    <div class="modal-body">
                                        Nom du partenaire : <input type="text" name="nomTypePartenaire" size="35">
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="valider">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
