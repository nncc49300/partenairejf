
<h3><i class="fa fa-angle-right"></i>Modifier un partenaire</h3>

<form action="index.php?uc=listePartenaire&action=modifierPartenaireScript&idPartenaire=<?php echo $idPartenaire ?>" method="POST" class="form-ajout form-inline">
	<div class="row">
		<div class="col-lg-12 centered">
			<h4>Informations du contrat</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="divider"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Section : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="section_id" id='section_id' required>
				<?php 
				foreach ($lesSections as $uneSection)
				{
				?>
					<option value="<?php echo $uneSection['idSection'];?>" <?php if($unPartenaire['section_id']==$uneSection['idSection']) echo "selected"; ?>> <?php echo $uneSection['nomSection']; ?></option>
				<?php
				}
				?>
				</select>
			</h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Saison : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="saison_id" id='saison_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesSaisons as $uneSaison)
					{
					?>
						<option value="<?php echo $uneSaison['idSaison'];?>"<?php if($unPartenaire['saison_id']==$uneSaison['idSaison']) echo "selected"; ?>> <?php echo $uneSaison['nomSaison']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Partenaire : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="typePartenaire_id" id='typePartenaire_id' required>
					<?php 
					foreach ($lesTypePartenaires as $unTypePartenaire)
					{
					?>
						<option value="<?php echo $unTypePartenaire['idTypePartenaire'];?>" <?php if($unPartenaire['typePartenaire_id']==$unTypePartenaire['idTypePartenaire']) echo "selected"; ?>> <?php echo $unTypePartenaire['nomTypePartenaire']; ?></option>
					<?php
					}
					?>
				</select>
			<h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Type de prestation : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="prestation_id" id='prestation_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesPrestations as $unePrestation)
					{
					?>
						<option value="<?php echo $unePrestation['idPrestation'];?>"<?php if($unPartenaire['prestation_id']==$unePrestation['idPrestation']) echo "selected"; ?>> <?php echo $unePrestation['nomPrestation']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Durée du contrat : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="duree_id" id='duree_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesDurees as $uneDuree)
					{
					?>
						<option value="<?php echo $uneDuree['idDuree'];?>" <?php if($unPartenaire['duree_id']==$uneDuree['idDuree']) echo "selected"; ?>> <?php echo $uneDuree['nomDuree']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Prestation : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="prestation" required value="<?php echo $unPartenaire['prestation']; ?>"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Montant promis: </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="montantPromis" value="<?php echo $unPartenaire['montantPromis']; ?>"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Montant Payé : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="montant" value="<?php echo $unPartenaire['montant']; ?>"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Date du paiement: </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="date" name="datePaiement" value="<?php echo $unPartenaire['datePaiement']; ?>"></h5>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-lg-12 centered">
			<h4>Informations générales</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="divider"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2"">
			<h5 class="label-place">Nom du contact : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="civiliteContact_id" id='civiliteContact_id' style="width: 25%;" required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesCivilites as $uneCivilite)
					{
					?>
						<option value="<?php echo $uneCivilite['idCiviliteContact'];?>"<?php if($unPartenaire['civiliteContact_id']==$uneCivilite['idCiviliteContact']) echo "selected"; ?>> <?php echo $uneCivilite['nomCiviliteContact']; ?></option>
					<?php
					}
					?>
				</select>
				<input type="text" name="nomContact" value="<?php echo $unPartenaire['nomContact']; ?>" style="width: 73%;"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Fonction du contact : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="fonctionContact" value="<?php echo $unPartenaire['fonctionContact']; ?>"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Adresse : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="adresse" value="<?php echo $unPartenaire['adresse']; ?>"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Code Postal : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="CP" pattern="[0-9]{5}" value="<?php echo $unPartenaire['CP']; ?>"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Ville : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="ville" value="<?php echo $unPartenaire['ville']; ?>"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Téléphone : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="tel" name="tel" pattern="[0-9]{10}" value="<?php echo $unPartenaire['tel']; ?>"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Mail : </h5>
		</div>
		<div class="col-lg-3">
			<h5><input type="email" name="mail" value="<?php echo $unPartenaire['mail']; ?>"></h5>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Remarques : </h5>
		</div>
		<div class="col-lg-3">
			<h5><textarea rows="4" cols="50" name="remarques" ><?php echo $unPartenaire['remarques']; ?></textarea></h5>
		</div>
	</div>

	<br>
	
	<div class="row">
		<div class="centered">
			<input type="submit" class="btn btn-success" value="Modifier" style="width: initial;">
		</div>
	</div>
</form>


