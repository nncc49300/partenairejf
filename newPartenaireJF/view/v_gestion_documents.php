<h3><i class="fa fa-angle-right"></i>Gestion documents</h3>
<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <section class="task-panel tasks-widget">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h4><i class="fa fa-tasks"></i> Modifier des documents</h4>
                    </div>
                    <br>
                </div>
                <div class="panel-body">
                    <div class="task-content">
                        <ul class="task-list">
                            <?php
                            foreach ($lesDocuments as $unDocument)
                            {
                                ?>
                                <li>
                                    <div class="task-title">
                                        <div class="todo-title">
                                            <span class="task-title-sp"><?php echo $unDocument['nomDocument'];?></span>
                                            <span class="badge bg-success"><?php echo $unDocument['anneeDocument'];?></span>
                                            <div class="pull-right hidden-phone">
                                                <a href="#modifierDocument<?php echo $unDocument['idDocument'];?>" data-toggle="modal"><button class="btn btn-success btn-xs">Modifier le document</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        <br>
                        <p style="color: #B60606; font-size: 120%; font-weight: bold">Pensez à faire une sauvegarde des fichiers avant de les modifier.</p>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /col-md-12-->
</div>

<div class="modal fade modal-ajoutTypePartenaire" id="modifierDocument1" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Modifier le contrat de mécénat</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-panel">
                        <form enctype="multipart/form-data" action="index.php?uc=gestion&action=modifier_document&idDocument=1" method="POST" class="form-horizontal style-form">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Fichier à modifier</label>
                                <div class="controls col-md-7">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-theme02 btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Selectionner fichier</span>&nbsp;&nbsp;
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Changer</span>
                                            <input type="file" class="default" name="fichierDocument"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="advanced_form_components.html#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Année</label>
                                <div class="controls col-md-7">
                                    <input type="text" name="anneeDocument">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls col-md-12 centered">
                                    <input type="submit" name="ajouter" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-ajoutTypePartenaire" id="modifierDocument2" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Modifier le contrat de sponsoring</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-panel">
                        <form enctype="multipart/form-data" action="index.php?uc=gestion&action=modifier_document&idDocument=2" method="POST" class="form-horizontal style-form">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Fichier à modifier</label>
                                <div class="controls col-md-7">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-theme02 btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Selectionner fichier</span>&nbsp;&nbsp;
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Changer</span>
                                            <input type="file" class="default" name="fichierDocument"  />
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="advanced_form_components.html#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Année</label>
                                <div class="controls col-md-7">
                                    <input type="text" name="anneeDocument">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls col-md-12 centered">
                                    <input type="submit" name="ajouter" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-ajoutTypePartenaire" id="modifierDocument3" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Modifier le dossier de  partenariat</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-panel">
                        <form enctype="multipart/form-data" action="index.php?uc=gestion&action=modifier_document&idDocument=3" method="POST" class="form-horizontal style-form">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Fichier à modifier</label>
                                <div class="controls col-md-7">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-theme02 btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Selectionner fichier</span>&nbsp;&nbsp;
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Changer</span>
                                            <input type="file" class="default" name="fichierDocument" />
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="advanced_form_components.html#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="text-align: right;">Année</label>
                                <div class="controls col-md-7">
                                    <input type="text" name="anneeDocument">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls col-md-12 centered">
                                    <input type="submit" name="ajouter" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
