
<h3><i class="fa fa-angle-right"></i>Nouveau contrat</h3>

<form action="index.php?uc=listePartenaire&action=ajoutPartenaireScript" method="POST" class="form-ajout form-inline">
	<div class="row">
		<div class="col-lg-12 centered">
			<h4>Informations du contrat</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="divider"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Section : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="section_id" id='section_id' required>
				<OPTION selected> </OPTION>
				<?php 
				foreach ($lesSections as $uneSection)
				{
				?>
					<option value="<?php echo $uneSection['idSection'];?>"> <?php echo $uneSection['nomSection']; ?></option>
				<?php
				}
				?>
				</select>
			</h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Saison : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="saison_id" id='saison_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesSaisons as $uneSaison)
					{
					?>
						<option value="<?php echo $uneSaison['idSaison'];?>"> <?php echo $uneSaison['nomSaison']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Partenaire : </h5>
		</div>
		<div class="col-lg-2 centered">
			<h5>
				<select name="typePartenaire_id" id='typePartenaire_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesTypePartenaires as $unTypePartenaire)
					{
					?>
						<option value="<?php echo $unTypePartenaire['idTypePartenaire'];?>"> <?php echo $unTypePartenaire['nomTypePartenaire']; ?></option>
					<?php
					}
					?>
				</select>
				<a href="#ajoutTypePartenaire" data-toggle="modal"><button class="btn btn-round btn-info btn-xs" style="margin-top: 5px;">Nouveau partenaire</button></a>
			<h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Type de prestation : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="prestation_id" id='prestation_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesPrestations as $unePrestation)
					{
					?>
						<option value="<?php echo $unePrestation['idPrestation'];?>"> <?php echo $unePrestation['nomPrestation']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Durée du contrat : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
				<select name="duree_id" id='duree_id' required>
					<OPTION selected> </OPTION>
					<?php 
					foreach ($lesDurees as $uneDuree)
					{
					?>
						<option value="<?php echo $uneDuree['idDuree'];?>"> <?php echo $uneDuree['nomDuree']; ?></option>
					<?php
					}
					?>
				</select>
			</h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Prestation : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="prestation" required></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Montant promis: </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="montantPromis"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Montant Payé : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="montant"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Date du paiement: </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="date" name="datePaiement"></h5>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-lg-12 centered">
			<h4>Informations générales</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="divider"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Nom du contact : </h5>
		</div>
		<div class="col-lg-2">
			<h5>
                <select name="civiliteContact_id" id='civiliteContact_id' style="width: 25%;" required>
                    <OPTION selected> </OPTION>
                    <?php
                    foreach ($lesCivilites as $uneCivilite)
                    {
                        ?>
                        <option value="<?php echo $uneCivilite['idCiviliteContact'];?>"> <?php echo $uneCivilite['nomCiviliteContact']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <input type="text" name="nomContact" required style="width: 73%;"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Fonction du contact : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="fonctionContact"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Adresse : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="adresse"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Code Postal : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="number" name="CP" pattern="[0-9]{5}"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Ville : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="text" name="ville"></h5>
		</div>
		<div class="col-lg-2">
			<h5 class="label-place">Téléphone : </h5>
		</div>
		<div class="col-lg-2">
			<h5><input type="tel" name="tel" pattern="[0-9]{10}"></h5>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Mail : </h5>
		</div>
		<div class="col-lg-3">
			<h5><input type="text" name="email"></h5>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-offset-1 col-lg-2">
			<h5 class="label-place">Remarques : </h5>
		</div>
		<div class="col-lg-3">
			<h5><textarea rows="4" cols="50" name="remarques" ></textarea></h5>
		</div>
	</div>
	
	<br>
	
	<div class="row">
		<div class="centered">
			<input type="submit" class="btn btn-success" value="Ajouter" style="width: initial;">
		</div>
	</div>
</form>

<div class="modal fade modal-ajoutTypePartenaire" id="ajoutTypePartenaire" role="dialog">
	<div class="modal-dialog modal-lg">
	  <!-- Modal content-->
	  <div class="modal-content">
	    <div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal">&times;</button>
	    	<h3 class="modal-title">Ajouter un partenaire</h3>
	    </div>
	    <div class="modal-body">
	    	<form action="index.php?uc=listePartenaire&action=ajoutTypePartenaireScript" method="POST">
	    		<center>
				<div class="row" style="margin-top: 1%;">
					<h5>Nom du partenaire :</h5>
				</div>
				<div class="row" style="margin-top: 1%;">
					<input type="text" name="nomTypePartenaire" required>
				</div>
				<div class="row" style="margin-top: 1%;">
					<input type="submit" value="ajouter">
				</div>
				</center>
				<div class="row">
					<br><br>
					<center><h5>Partenaire existants : </h5></center>
					<br>
					<?php 
					$lenghtColumn = round(sizeof($lesTypePartenaires)/3);
					$dividedArray = array_chunk($lesTypePartenaires, $lenghtColumn);

					for($i=0;$i<3;$i++){
					?>
						<div class="col-sm-4">
						<?php
						foreach ($dividedArray[$i] as $unTypePartenaire){
							echo $unTypePartenaire['nomTypePartenaire']."<br>";
						} 
						?>
						</div>
						<?php
					}
					?>
				</div>
			</form>
	    </div>
	  </div>
	</div>
</div>
