  
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="index.php"><img src="img/jf_logo.jpg" class="img-circle" width="80"></a></p>
          <li class="mt">
            <a <?php if($uc == "listePartenaire"){echo "class=\"active\"";} ?> href="index.php?uc=listePartenaire">
              <i class="fa fa-th"></i>
              <span>Liste des contrats</span>
              </a>
          </li>
          <li class="mt">
            <a <?php if($uc == "documents"){echo "class=\"active\"";} ?>  href="index.php?uc=documents">
              <i class="fa fa-book"></i>
              <span>Documents</span>
            </a>
          </li>
          <li class="mt">
            <a <?php if($uc == "statistiques"){echo "class=\"active\"";} ?> href="index.php?uc=statistiques">
              <i class="fa fa-bar-chart"></i>
              <span>Statistiques</span>
              </a>
          </li>
            <?php
            if(verifLevelAdmin(1)) {
                ?>
            <li class="mt sub-menu">
                <a <?php if($uc == "gestion"){echo "class=\"active\"";} ?> href="javascript:;">
                    <i class="fa fa-desktop"></i>
                    <span>Gestion</span>
                </a>
                <ul class="sub">
                    <li>
                        <a href="index.php?uc=gestion&action=gestion_partenaires">
                            <span>Partenaires</span>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?uc=gestion&action=gestion_documents">
                            <span>Documents</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            }
            ?>
            <!--
            <li class="mt">
                <a <?php //if ($uc == "gestion") { echo "class=\"active\""; } ?> href="index.php?uc=gestion">
                    <i class="fa fa-gears"></i>
                    <span>Gestion</span>
                </a>
            </li>
            -->
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
