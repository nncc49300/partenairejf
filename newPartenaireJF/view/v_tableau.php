﻿<script type="text/javascript">
	function export2CSV(){

	  $("#table2excel").table2excel({
	    // exclude CSS class
	    exclude: ".noExl",
	    name: "Worksheet Name",
	    filename: "tableau.xls" //do not include extension
	  });

	}
</script>

<div class="row">
	<div class="col-sm-6">
		<h3><i class="fa fa-angle-right"></i>Liste des contrats</h3>
	</div>
	<div class="col-sm-6" style="text-align: right; margin: 1% 0 0 0;">
		<a href="index.php?uc=listePartenaire&action=ajoutPartenaire"><button class="btn btn-round btn-success">Nouveau contrat</button></a>
	</div>
</div>

<div class="row mt">
  <div class="col-md-12">
    <div class="content-panel">
      <table class="table table-striped table-advance table-hover">
        <thead>
          <tr>
          	<th>Détails</th>
			<th>
				<select name="section_id" id='section_id' onchange="javascript:location.href='index.php?uc=listePartenaire&action=affichageTableau&filtre=section&value='+this.value;">
					<OPTION value="0" <?php if($_SESSION['filtreSection']=="0"){echo "selected";} ?>>Sections</OPTION>
					<?php 
					foreach ($lesSections as $uneSection)
					{
					?>
						<option value="<?php echo $uneSection['idSection'];?>" <?php if(isset($_SESSION['filtreSection'])){if($_SESSION['filtreSection']==$uneSection['idSection']){echo "selected";}} ?>> <?php echo $uneSection['nomSection']; ?></option>
					<?php
					}
					?>
				</select>
			</th>
			<th>
				<select name="typePartenaire_id" id='typePartenaire_id' onchange="javascript:location.href ='index.php?uc=listePartenaire&action=affichageTableau&filtre=typePartenaire&value='+this.value;">
					<OPTION value="0" <?php if($_SESSION['filtreTypePartenaire']=="0"){echo "selected";} ?>>Partenaires</OPTION>
					<?php 
					foreach ($lesTypePartenaires as $unTypePartenaire)
					{
					?>
						<option value="<?php echo $unTypePartenaire['idTypePartenaire'];?>" <?php if(isset($_SESSION['filtreTypePartenaire'])){if($_SESSION['filtreTypePartenaire']==$unTypePartenaire['idTypePartenaire']){echo "selected";}} ?>> <?php echo $unTypePartenaire['nomTypePartenaire']; ?></option>
					<?php
					}
					?>
				</select>
			</th>
			<th>
				<select name="saison_id" id='saison_id' onchange="javascript:location.href ='index.php?uc=listePartenaire&action=affichageTableau&filtre=saison&value='+this.value;">
					<OPTION value="0" <?php if($_SESSION['filtreSaison']=="0"){echo "selected";} ?>>Saisons</OPTION>
					<?php 
					foreach ($lesSaisons as $uneSaison)
					{
					?>
						<option value="<?php echo $uneSaison['idSaison'];?>" <?php if(isset($_SESSION['filtreSaison'])){if($_SESSION['filtreSaison']==$uneSaison['idSaison']){echo "selected";}}?>> <?php echo $uneSaison['nomSaison']; ?></option>
					<?php
					}
					?>
				</select>
			</th>
			<th>Type de prestation</th>
			<th>Prestation</th>
			<th>Date du paiement</th>
			<th style="width: 7%">Actions</th>
		</tr>
        </thead>
        <tbody>
			<?php 
			foreach ($lesPartenaires as $unPartenaire)
			{ ?>
                <tr>
                    <td style="text-align: center; vertical-align: middle;"><a href="#detailPartenaire<?php echo $unPartenaire['idPartenaire']; ?>" data-toggle="modal"><img src="img/loupe.png" class="img-loupe"></a></td>
                    <td style="vertical-align: middle;"><?php echo $unPartenaire['nomSection']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $unPartenaire['nomTypePartenaire']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $unPartenaire['nomSaison']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $unPartenaire['nomPrestation']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $unPartenaire['prestation']; ?></td>
                    <td style="vertical-align: middle;"><?php $date=$unPartenaire['datePaiement']; if($date=="0000-00-00"){echo "";} else{$dateFormat = date_create($date); echo date_format($dateFormat, 'd/m/Y');} ?></td>
                    <td style="vertical-align: middle;">
                        <a href="index.php?uc=listePartenaire&action=modifierPartenaire&idPartenaire=<?php echo $unPartenaire['idPartenaire']; ?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                        <?php if(verifLevelAdmin(1)) { ?>
                            <a href="#supprimerPartenaire<?php echo $unPartenaire['idPartenaire']; ?>" data-toggle="modal"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                        <?php } ?>
                    </td>
                </tr>
                <!-- Modal détails partenaire-->
                <div class="modal fade modal-detailPartenaire" id="detailPartenaire<?php echo $unPartenaire['idPartenaire']; ?>" role="dialog">
                    <div class="modal-dialog modal-lg">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo $unPartenaire['nomSection']; ?> - <?php echo $unPartenaire['nomTypePartenaire']; ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12"><h4>Informations contrat</h4></div>
                            </div>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Section : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomSection']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Saison : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomSaison']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Partenaire : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomTypePartenaire']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Type de prestation : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomPrestation']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Durée du contrat : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomDuree']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Prestation : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['prestation']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Montant promis : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['montantPromis']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Montant payé : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['montant']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Date du paiement : </h5></div>
                                <div class="col-lg-10"><h5><?php $date=$unPartenaire['datePaiement']; if($date=="0000-00-00"){echo "";} else{$dateFormat = date_create($date); echo date_format($dateFormat, 'd/m/Y');} ?></h5></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12"><h4>Informations générales</h4></div>
                            </div>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Nom contact : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['nomCiviliteContact']." ".$unPartenaire['nomContact']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Fonction contact : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['fonctionContact']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Adresse : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['adresse']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">CP : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['CP']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Ville : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['ville']; ?></h5></div>
                                <div class="col-lg-2"><h5 class="label-place">Téléphone : </h5></div>
                                <div class="col-lg-4"><h5><?php echo $unPartenaire['tel']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Mail : </h5></div>
                                <div class="col-lg-10"><h5><?php echo $unPartenaire['mail']; ?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><h5 class="label-place">Remarques : </h5></div>
                                <div class="col-lg-10"><h5><?php echo $unPartenaire['remarques']; ?></h5></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <a href="index.php?uc=listePartenaire&action=modifierPartenaire&idPartenaire=<?php echo $unPartenaire['idPartenaire']; ?>"><button type="button" class="btn btn-warning">Modifier</button></a>
                            <?php if(verifLevelAdmin(1)) { ?>
                                <a href="#supprimerPartenaire<?php echo $unPartenaire['idPartenaire']; ?>" data-toggle="modal"><button class="btn btn-danger">Supprimer</button></a>
                            <?php } ?>
                          <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- Modal supprimer partenaire (admin only)-->
                <?php if(verifLevelAdmin(1)) { ?>
                    <div class="modal fade modal-supprimerPartenaire" id="supprimerPartenaire<?php echo $unPartenaire['idPartenaire']; ?>" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Confirmation</h4>
                                </div>
                                <div class="modal-body">
                                    Voulez-vous vraiment supprimer ce contrat ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
                                    <a href="index.php?uc=listePartenaire&action=supprimerPartenaire&idPartenaire=<?php echo $unPartenaire['idPartenaire']; ?>">
                                        <button type="button" class="btn btn-danger">Supprimer</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
			<?php
			}
			?>
        </tbody>
      </table>

    </div>
  </div>
</div>